What is BOSS
================================

BOSS uses Bayesian optimization (BO) active learning method to
iteratively build a surrogate model of the target property (for
example potential energy of a system). BO employs uncertainty-led
exploration/exploitation sampling strategy, which allows quick
convergence of the model with a modest number of data acquisitions.
The target property is optimized via automatic detection of global
and local minima, which are reliably extracted from the N-dimensional
surrogate model. The model is typically constructed in a phase space
of under 10 dimensions. This is achieved for example in atomistic
systems by describing the structure with chemical *building blocks*
(see :ref:`Example 1<userfn_example_1>`). 

The main features of BOSS include automatic Gaussian process (GP)
hyperparameter optimization for reliable model convergence, various
acquisition functions for different BO tasks, and a wide range of
postprocessing tools to extract information from the model. With a
simple *user function* Python script, BOSS can be interfaced to many
different kinds of simulation software and experimental procedures.

Capabilities at a glance
++++++++++++++++++++++++

The following sections outline the basic features of BOSS, such as
building the model with active learning, data-mining the model via
postprocessing, and other practical features.

.. toctree::
   :maxdepth: 4

   features/features_AL
   features/features_PP
   features/features_practical

Further information
+++++++++++++++++++

Further information is provided at the `BOSS main website <https://sites.utu.fi/boss/>`_ with various different `research examples <https://sites.utu.fi/boss/research/>`_. The BOSS code is available for download in `GitLab <https://gitlab.com/cest-group/boss>`_.


Authors
+++++++

BOSS was conceived by Milica Todorović (University of Turku) and Patrick
Rinke (Aalto University) in collaboration with Jukka Corander
(University of Helsinki/University of Oslo) and Michael Gutmann
(University of Edinburgh). The original BOSS prototype MATLAB code was developed by MT and MG. 

The BOSS python package is under continuous development at Aalto University
by the `Computational Electronic Structure Theory (CEST) <https://www.aalto.fi/department-of-applied-physics/computational-electronic-structure-theory-cest>`_ group and at University of Turku.

See `BOSS people  <https://sites.utu.fi/boss/people/>`_ for the full list of authors.
