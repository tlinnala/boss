Batch acquisitions
==================
Note: you can also download this tutorial as a :download:`python script <pyfiles/batches.py>` or a :download:`notebook <notebooks/batches.ipynb>`.

In this notebook we demonstrate how to use batch acquisitions, which refers to producing
multiple acquisitions per iteration, which can significantly speed up the BO process if
the user function can be evaluated in parallel, .e.g, if extra resources are available on
a super computer, or samples can be processed together in a lab.

.. code-block:: python

    import numpy as np
    from boss.bo.bo_main import BOMain
    from boss.pp.pp_main import PPMain

    def f(x):
        return np.sin(x) + 1.5*np.exp(-(x-4.3)**2)

Batch acquisitions can be turned on using the ``batchtype`` keyword. The default value
of this keyword is sequential, which selects a standard sequential acquisition manager
that produces 1 acquisition per BO iteration. To use batch acquisitions, we can set the ``batchtype`` 
to kriging_believer, or kb for short, which will select a kriging-believer style acquisition manager.
This is a rather slow method for batch acquisitions but the fidelity is quite high. The number of
acquisitions produced each iteration can now be set with the ``batchpts`` keyword.

.. code-block:: python

    bo = BOMain(
        f,
        bounds=[0, 7],
        initpts=4,
        iterpts=5,
        batchtype="kb",
        batchpts=2,
    )
    res = bo.run()

Looking at the results we can see that 2 acquisitions were produced per iteration. For example 
in the second iteration:

.. code-block:: python

    print(res.select("X", itr=2))

Post-processing will work as usual:

.. code-block:: python

    pp = PPMain(res, pp_acq_funcs=True, pp_models=True)
    pp.run()
