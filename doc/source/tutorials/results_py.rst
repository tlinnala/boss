.. _results_py:

Results
=======
Note: you can also download this tutorial as a :download:`python script <pyfiles/results.py>` or a :download:`notebook <notebooks/results.ipynb>`.

This tutorial showcases how to use the ``BOResults`` object to get information
about a BOSS run after it has concluded. First, we quickly optimize the 
Forrester function again to get some data to play with.

.. code-block:: python

    import numpy as np
    from boss.bo.bo_main import BOMain

    def func(X):
        x = X[0, 0]
        return (6 * x - 2) ** 2 * np.sin(12 * x - 4)

    bounds = np.array([[0.0, 1.0]])
    bo = BOMain(func, bounds, kernel="rbf", initpts=5, iterpts=10)
    res = bo.run()

The ``BOResults`` object gives easy access to a number of important
results from the run,  by default the following information is stored:

* X and Y data (initial data + acquistions)

* Model hyperparameters

* Predicted global min, min location and variance

The raw results data is stored in a dict. To see what data is
available, we can look at the keys of this dict

.. code-block:: python

    print(res.data.keys())

The easiest way of accessing data is ``BOResults.select`` method, which
should be preferred over directly accessing the raw data.

.. code-block:: python

    X = res.select('X')
    Y = res.select('Y')
    mu_glmin = res.select('mu_glmin', -1)  # global min prediction from the last iteration
    x_glmin = res.select('x_glmin', -1)  # global min location prediction
    nu_glmin = res.select('nu_glmin', -1)  # global min variance prediction

Here, the second argument to the select method is the iteration from
which to take the result. Note that the iteration number wraps around
negative numbers like numpy array indices, hence -1 means the final
iteration. We use the convention that the 0th iteration refers to to
the initial data and positive iterations refers to actual BO steps. A
few more examples:

.. code-block:: python

    X_init = res.select('X', 0)  # Selects all initial X-data.
    params = res.select('model_params', [1, 3])  # Selects model params data from iters 1 and 3.

In additional data selection, ``BOResults`` provides a number of other
useful methods such as:

.. code-block:: python

    model = res.reconstruct_model(3)  # Reconstructs the GP model from iter 3.
    acqfn = res.reconstruct_acq_func(2)  # Reconstructs the acquisition function from iter 2.
    x_best, y_best = res.get_best_acq(5)  # Gets the best acq from first 5 iters.
    y_range = res.get_est_yrange(4)  # Gets the estimated range of y after iter 4.

Although data access through the select method is recommended, the raw
data stored in the BOResults can be accessed getitem-style:

.. code-block:: python

    X = res['X']   # All X data

Depending on which BOSS settings are used, certain results may not be
recorded every iteration. Such results, which includes the model
hyperparameters and predicted minimum information, is stored in a
special object called a SparseList. This list-like object has an
internal dict structure that pairs iteration numbers to result values
only when the result is actually calculated.  To illustrate its usage,
we run BOSS again but only predict the global min every other
iteration:

.. code-block:: python

    bo = BOMain(func, bounds, kernel="rbf", initpts=5, iterpts=10, minfreq=2)
    res = bo.run()
    mu_glmin = res['mu_glmin']
    print(type(mu_glmin))  # SparseList, not an array
    print(mu_glmin[0], mu_glmin[1], mu_glmin[2], mu_glmin[3])  # min only available from every other iter
