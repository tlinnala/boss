Cost functions
==============
Note: you can also download this tutorial as a :download:`python script <pyfiles/costs.py>` or a :download:`notebook <notebooks/costs.ipynb>`.

In this notebook we demonstrate how to apply cost functions to an acquisition function. We define a
cost function as an arbitrary function that is used to modify the acquisition function. Currently, 
BOSS allows for additive or divisive (i.e. dividing) cost functions. For simplicity, we will use 
a 1D example although the feature can be applied in any number of dimensions.

.. code-block:: python

    import numpy as np
    from boss.bo.bo_main import BOMain
    from boss.pp.pp_main import PPMain

    def f(x):
        return np.sin(x) + 1.5*np.exp(-(x-4.3)**2)

Similarly to the user function, the cost must be defined as a Python function. In addition,
it must be vectorized and return it's own gradient. By vectorized, we mean that the function 
must be able to operate an (n, d)-shaped array x of inputs where d is the dimension (here d=1)
and n is an arbitrary positive integer. For such an input array, a corresponding (n, 1)-shaped output array
containing the cost values must be calculated and returned together with the gradient (which
will have the same shape as x).


.. code-block:: python

    def cost(x):
        grad = np.zeros(x.shape)
        cost = np.zeros((x.shape[0], 1))
        cost[x > 5] = 10
        return cost, grad

We then pass the cost to BOSS using the ``costfn`` keyword and specify that we would like the cost
to be added to the acquisition via the ``costtype``` keyword. 

.. code-block:: python

    bo = BOMain(
        f,
        costfn=cost,
        costtype='add', # the other option is divide
        bounds=[0, 7],
        seed=25,
        initpts=2,
        iterpts=15,
    )
    res = bo.run()

In this case, our cost function is a step of height 10 located at x>5, and hence adding this
to the acquisition function will effectively prevent all acquisitions in the x>5 region.
This can be verified by running postprocessing and checking the model and acquisition function
graphs generated under ``postprocessing/graphs_models`` and ``postprocessing/graphs_acqfns``.

.. code-block:: python

    pp = PPMain(res, pp_acq_funcs=True, pp_models=True)
    pp.run()

Note that the applications of cost functions is much broader than just preventing acquisitions in certain regions.
A moderate to low cost can, e.g., be use to decrease acquisitions in a region where evaluating the objective function is extra expensive.
