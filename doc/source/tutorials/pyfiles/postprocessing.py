# # Postprocessing

# After an optimization has finished, BOSS has the capability of automatically plotting
# a selection of the most important results as well as creating corresponding data dump files
# for user defined postprocessing. This tutorial shows how to invoke BOSS' postprocessing
# capabilities directly from python. We continue from the example of the optimization
# of the Forrester function previously demonstrated in the `basic.ipynb` tutorial.

import numpy as np
from boss.bo.bo_main import BOMain

# Import the PPMain object to run postprocessing later.
from boss.pp.pp_main import PPMain


def func(X):
    x = X[0, 0]
    return (6 * x - 2) ** 2 * np.sin(12 * x - 4)


bounds = np.array([[0.0, 1.0]])

bo = BOMain(func, bounds, kernel="rbf", initpts=5, iterpts=15)
res = bo.run()

# After the optimization has finished, we create the main postprocessing object, named `PPMain`,
# using the `BOResults` object handed to us at the end of the optimization run. We also have
# the option to again specify any BOSS keywords related to postprocessing (in case we forgot to
# during the initialization of the `BOMain` object or if we are restarting from a run that did
# not include post-processing)

# Specify keywords to include postprocessing plots and data dumps
# for both the model and the acquisition function.
pp = PPMain(res, pp_models=True, pp_acq_funcs=True)
pp.run()

# After the postprocessing has finished, the resulting plots and data dumps can be found under the
# newly created `postprocessing` folder. Note that if a folder with this name already exists in the
# current working directory all of its contents will be overwritten!
