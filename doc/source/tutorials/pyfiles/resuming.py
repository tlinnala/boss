# # Resuming

# If we run BOSS and find that the results are not satisfactory, the optimization
# can easily be resumed as long as the original BOMain object has not gone out of scope.
# To illustrate this, we consider a 2D function, which has an approximate global minimum
# y = 1.463 at x = (-4.000, -3.551).

from boss.bo.bo_main import BOMain
from boss.pp.pp_main import PPMain


def func_2d(X):
    x1 = X[0, 0]
    x2 = X[0, 1]
    y = 0.01 * ((x1 ** 2 + x2 - 11) ** 2 + (x1 + x2 ** 2 - 7) ** 2 + 20 * (x1 + x2))
    return y


bo = BOMain(
    func_2d,
    bounds=[[-5.0, 5.0], [-5.0, 5.0]],
    initpts=5,
    iterpts=10,
)
res = bo.run()
pred_min = res.select('mu_glmin', -1)
x_pred_min = res.select('x_glmin', -1)
print("Predicted global min: {} at x = {}".format(pred_min, x_pred_min))

# Here, we only did 10 BO iterations, which is on the low side
# for a non-trivial 2D problem. We can improve on our result by resuming our optimization
# and adding, say 20 additional iterations. To do so, we simply invoke the `BOMain.run` method
# again and specify the new total number of iterations = 10 + 20 = 30:

res = bo.run(iterpts=30)
pred_min = res.select('mu_glmin', -1)
x_pred_min = res.select('x_glmin', -1)
print("Predicted global min after resuming: {} at x = {}".format(pred_min, x_pred_min))
pp = PPMain(res, pp_models=True, pp_acq_funcs=True)
pp.run()
