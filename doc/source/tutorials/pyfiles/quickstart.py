# # Quickstart

# We illustrate the basic usage by minimizing the Forrester function
# f(x) = (6x - 2)<sup>2</sup> sin(12x - 4)  on the interval [0, 1].

import numpy as np
from boss.bo.bo_main import BOMain
from boss.pp.pp_main import PPMain

# The first step to running BOSS typically consists of defining an objective function and
# the optimization bounds, where the latter should be specified as a hypercube.
# For the Forrester problem, we define the function and bounds as follows


def func(X):
    x = X[0, 0]
    return (6 * x - 2) ** 2 * np.sin(12 * x - 4)

bounds = np.array([[0.0, 1.0]])

# Note that BOSS expects the objective function to take a single 2D numpy array
# as argument and return a scalar value (this behaviour can be modified).
# Next, we import BOMain, which will be used to launch and configure the optimization.
# When creating this object we can supply any number of BOSS *keywords*,
# these are used to provide essential input information and modify BOSS's behavior.
# In the following, only a minimal set of keywords are provided for brevity.

bo = BOMain(func, bounds, kernel="rbf", initpts=5, iterpts=10)

# We are now ready to start the optimization. Once finished, a `BOResults` object
# that provides easy access to several resulting quantities is returned. In addition,
# a summary of the optimization and restart data is written to separate files, by default
# named `boss.out` and `boss.rst`, respectively.

res = bo.run()

# The `BOResults object gives easy access to the most important results from the run.
# For instance, we can get the predicted global minimum from the last iteration:

print('Predicted global min: ', res.select('mu_glmin', -1)) 

# A more detailed description of the `BOResults` object can be found in the corresponding Results tutorial.
# BOSS also supports automatic post-processing, generating useful plots and data dumps in a directory 
# called `postprocessing`.

pp = PPMain(res, pp_models=True, pp_acq_funcs=True)
pp.run()
