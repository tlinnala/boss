# # User function symmetry

# In this tutorial we show how to create BOSS user functions that 
# return extra (x, y) data for function evaluations at custom locations in addition to the usual acquisitions.
# This can be used, e.g., to exploit symmetries in the user function. 

import numpy as np
from boss.bo.bo_main import BOMain
from boss.pp.pp_main import PPMain

# Consider the toy problem of minimizing the symmetric function f(x) = x<sup>2</sup>cos(2x) on the interval [-3, 3].
# Since we know that f(x) = f(-x), we can create a BOSS user function that for each acquisition x also evaluates the
# function at -x for free. 
# To ensure that BOSS knows about your custom evaluations, your function must return both a 2D-array of x-locations
# and a 1D array with the corresponding y-values.

def func_sym(X):
    # recall boss always passes the inputs as a 2d array
    x1 = X[0, 0]
    x2 = -x1
    y1 = x1**2 * np.cos(2*x1)
    # We must return the x-locations as rows in a 2D array.
    X = np.array([x1, x2])[:, None]
    # The corresponding evaluations must must be returned in a 1D array.
    y = np.array([y1, y1])
    return X, y

# No special keyword is required to use this feature of BOSS, the 
# extra user function evaluations will be detected automatically.
# Extra user function evaluations are not taken into account for the purpose of specifying the number of initial points and iterations.
# Thus, in the present example we specify 3 initial points and 5 iterations and ourfunction adds 1 extra evaluation each call, hence the final model will be based on
# a total 16 data points.

bounds = np.array([[-3., 3.]])
bo = BOMain(
    func_sym, 
    bounds,    
    kernel='rbf',
    initpts=3,
    iterpts=5,
)
res = bo.run()
print(f"pred. x-min: {res.select('x_glmin', -1)}")
print(f"pred. f-min: {res.select('mu_glmin', -1)}")
pp = PPMain(res, pp_models=True, pp_acq_funcs=True)
pp.run()

# Note that in this simple example we could have taken the symmetry into account by simply limiting the bounds
# to [0, 3], yet there are many applications where this is difficult or not desirable, e.g., when we want 
# to preserve the periodicity of the function.
