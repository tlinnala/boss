# # Heteroscedastic noise

# This tutorial demonstrates how treat heteroscedastic noise problems with BOSS. Heteroscedasticity implies that the noise can change as a function of the inputs X or singal Y.   

import numpy as np
from boss.bo.bo_main import BOMain


# Noisy objective function, in this case, the noise is proportional to the
# absolute value of the objective function

def func(X):
    x = X[0, 0]
    noise = np.random.normal(loc=0.0, scale=0.1)
    return (6 * x - 2)**2 * np.sin(12*x - 4)*(1.0 + noise)


# We need to provide a function to estimate noise associated with observations,
# model data/parameters can be accessed through the `kwargs` dict:

def hsc_noise(hsc_args, **kwargs):
    Y = kwargs["Y"]
    noise_array = hsc_args[0] * np.abs(Y)
    return noise_array


# Custom arguments required by our noise estimation function can be passed
# to BOSS via the `hsc_args`:

hsc_args = [0.1]

bo = BOMain(
    func,
    bounds=np.array([[0.0, 1.0]]),
    yrange=[-1.0, 1.0],
    kernel="rbf",
    initpts=5,
    iterpts=10,
    hsc_noise=hsc_noise,
    hsc_args=hsc_args,
)

res = bo.run()
