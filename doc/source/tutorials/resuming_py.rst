.. _resuming_py:

.. NOTE: THIS TEXT IS ALREADY COMBINED TO THE "RESTARTING_PY" SECTION,
   CAN BE DELETED

Resuming
=============
Note: you can also download this tutorial as a :download:`python script <pyfiles/resuming.py>` or a :download:`notebook <notebooks/resuming.ipynb>`.

If we run BOSS and find that the results are not satisfactory, the optimization
can easily be resumed as long as the original BOMain object has not gone out of scope.

To illustrate this, we consider a 2D function, which has an approximate global minimum :math:`y = 1.463` at :math:`x = (-4.000, -3.551)`.

.. code-block:: python

    from boss.bo.bo_main import BOMain
    from boss.pp.pp_main import PPMain

    def func_2d(X):
        x = X[0, 0]
        y = X[0, 1]
        z = 0.01 * ((x ** 2 + y - 11) ** 2 + (x + y ** 2 - 7) ** 2 + 20 * (x + y))
        return z

    bo = BOMain(
        func_2d,
        bounds=[[-5.0, 5.0], [-5.0, 5.0]],
        initpts=5,
        iterpts=10
    )
    res = bo.run()
    print('Predicted global min: {} at x = {}'.format(res.select('mu_glmin', -1), res.select('x_glmin', -1)))

Here, we only did 10 BO iterations, which is on the low side for a non-trivial 2D problem. We can improve on our result by resuming our optimization and adding, say 20 additional iterations. To do so, we simply invoke the ``BOMain.run`` method again
and specify the new total number of iterations = 10 + 20 = 30:

.. code-block:: python

    res = bo.run(iterpts=30)
    print('Predicted global min after resuming: {} at x = {}'.format(res.fmin, res.xmin))
    # run postprocessing
    pp = PPMain(res, pp_models=True, pp_acq_funcs=True)
    pp.run()
