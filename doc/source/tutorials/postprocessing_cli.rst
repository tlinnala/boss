.. _postprocessing_cli:

Post-processing
====================

Postprocessing routines of BOSS allow to monitor convergence and
extract important information from the simulation. By postprocessing a
BO run, it is possible to:

* visualize data acquisition trends, simulation convergence and
  hyperparameters;

* extract snapshots of the surrogate models. In multidimensional runs,
  models can be extracted in 2D slices (with other variables at global
  minimum values, or specified values) or in 1D line profiles;

* compute the true function f(x) on a grid across the search space
  (this can be used to validate BO results in low dimensions);

* evaluate the true function at the global minimum locations
  throughout the BO simulation (used to evaluate the convergence and
  accuracy of the surrogate model);

* extract the locations of all local minima from the surrogate model
  refined by the BO simulation.

By default all BO iterations are postprocessed, but it is also
possible to select only a few. For more details please consult the
:ref:`post-processing<Data post-processing>` section and the
:ref:`post-processing keywords<keywords_pp>`.

---

With the command line interface, BO and postprocessing (PP)
simulations can be carried out separately. In this sense, BOSS
supports several different simulation types:

* joint BO and PP (PP continues automatically after BO is complete).
  Input file **myinput** must contain PP keywords. 

::

	> boss op myinput

* running pure BO, followed by pure PP (useful for local data analysis
  after long HPC BOSS runs). Here the input file **myinput** may or
  may not feature PP keywords, the 'o' running option ensures they are
  ignored. For the PP run, however, the resulting **boss.rst** must
  have the PP keywords (the file can be modified and saved as e.g.
  **mod_boss.rst**). Both **boss.out** and **boss.rst** are needed for
  pure postprocessing.

::

	> boss o myinput
	> boss p mod_boss.rst boss.out


