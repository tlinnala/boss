Gradient observations
=====================
Note: you can also download this tutorial as a :download:`python script <pyfiles/quickstart.py>` or a :download:`notebook <notebooks/quickstart.ipynb>`.

We will be minimizing the Forrester function :math:`f(x) = (6x - 2)^2 \sin(12x - 4)`  on the interval :math:`0 \le x \le 1`. 

.. code-block:: python

    import numpy as np
    from boss.bo.bo_main import BOMain

The user function must return the function value and gradient in tuple
(y, dy) format:

.. code-block:: python

    def func(X):
        x = X[0, 0]
        y = (6 * x - 2) ** 2 * np.sin(12 * x - 4)
        dy = 12 * (6 * x - 2) * (np.sin(12 * x - 4) + (6 * x - 2) * np.cos(12 * x - 4))
        return y, dy

    bounds = np.array([[0.0, 1.0]])

We create the BOMain object with the keyword ``ygrad`` set to ``True``. The
process is then run normally (NOTE: including gradient observations will slow
down BOSS, especially at higher dimensions).

.. code-block:: python

    bo = BOMain(
        func,
        bounds,
        ygrad=True,
        kernel="rbf",
        initpts=3,
        iterpts=7,
   )
   res = bo.run()

Including gradient observations generally lowers the number of iterations required for a good prediction of the global minimum.

.. code-block:: python

    print('Predicted global min: ', res.select('mu_glmin', -1))

    # Note that the observed gradients are concatenated to function values in the
    # results object:

    print(res.select('Y'))

Please see the other tutorial notebooks for more detailed descriptions of results and postprocessing.
