.. BOSS documentation master file, created by
   sphinx-quickstart on Tue Dec 12 14:46:05 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. toctree::
   :maxdepth: 4
   :hidden:

   whatis
   installation
   tutorials
   manual
   interface


.. figure:: figures/boss_method.png
   :scale: 38 %
   :alt: BOSS examples

Bayesian Optimization Structure Search (BOSS) is an active machine
learning technique for accelerated global exploration of property
phase space. It is designed to facilitate machine learning in
computational and experimental natural sciences. This documentation
presents the :ref:`installation <Installation>` and usage of BOSS via
hands-on :ref:`Tutorials <Tutorials>` and the :ref:`Manual <Manual>`.
More information and research examples can be found at the BOSS
`public website <https://sites.utu.fi/boss/>`_. The code is available
for download in the BOSS `GitLab repository
<https://gitlab.com/cest-group/boss>`_.


