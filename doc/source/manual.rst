Manual
======
This section provides a more in-depth explanation of BOSS inputs and outputs.
We describe how to run and monitor simulation convergence. BOSS defaults are 
set to faciliate easy BO for non-experts, but through the list of keywords it 
is possible to fine-tune all aspects of the simulation.

To establish the BOSS nomenclature, terminology and names of key variables
related to BO, we provide a set of theoretical definitions.
We encourage the reader to refer back to these definitions when reading the manual.

.. toctree::
   :maxdepth: 4
   
   manual/definitions
   manual/userfn
   manual/usage
   manual/postprocessing
   manual/tips_and_tricks
   manual/keywords
