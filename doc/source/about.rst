About
=====

Authors
-------
BOSS was conceived by Milica Todorović (University of Turku) and Patrick
Rinke (Aalto University) in collaboration with Jukka Corander
(University of Helsinki/University of Oslo) and Michael Gutmann
(University of Edinburgh). The original BOSS prototype MATLAB code was developed by MT and MG. 

The BOSS python package is under continuous development at Aalto University
by the `Computational Electronic Structure Theory (CEST) <https://www.aalto.fi/department-of-applied-physics/computational-electronic-structure-theory-cest>`_ group and at University of Turku. Past and present members of development team include

* Ville Parkkinen
* Henri Paulamäki
* Arttu Tolvanen
* Ulpu Remes
* Nuutti Sten
* Emma Lehto
* Manuel Kuchelmeister
* Tuomas Rossi
* Mikael Granit
* Ransell D'souza
* Armi Tiihonen
* Matthias Stosiek
* Joakim Löfgren (maintainer)
* Milica Todorović (team lead)

with contributions from

* Jarno Rantaharju 
* Ester Koistinen (Royal Holloway)
* Harshit Mahapatra (Aarhus University)

Contact
-------
If you encounter issues with the software, or want to suggest improvements, please use the `issues feature <https://gitlab.com/cest-group/boss/issues>`_ of gitlab where the code is hosted. This way the discussion is publicly available and the discussion history is preserved.

If you want to contact the authors on other matters, use the following email: milica.todorovic@utu.fi 

License
-------

The software is licensed under the Apache License, Version 2.0

Funding
-------

This project has received funding from the Academy of Finland under project no. 316601 in the Artificial Intelligence in Physical Sciences and Engineering (AIPSE) programme.

