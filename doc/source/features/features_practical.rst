.. _features_practical:

Practical features
==================

.. figure:: ../figures/features_practical.png
   :width: 100.0%

   Fig. 1: BOSS internal structure

Other features that make BOSS a robust and user friendly method:

- interface choices: Python API and command line (CLI)
- BO restart functionality for queue-based HPC computing
- versatile objective function: integrate with simulations or experiments
- out-of-the-box postprocessing graphics

