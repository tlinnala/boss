Tutorials
=========

BOSS supports running either via a simple command-line interface (CLI)
or directly in Python scripts using the Python application programming
interface (API). Several tutorials are available below for both modes
of running, with emphasis on the practicalities of using the code. In
both modes, BOSS settings such as the number of iterations, key
algorithms, variable input and so on, can be conveniently controlled
through BOSS :ref:`keywords<Keywords>`, some of which are necessary to
start the active learning simulation. 

Command-line interface and Python API
+++++++++++++++++++++++++++++++++++++

The BOSS CLI is provided by an executable called ``boss`` that is
automatically generated during the :ref:`installation<Installation>`
process. The function to be optimized must be defined in Python, but
once this is done, the user is free to run BOSS from the command line
without interacting with Python at all. Conversely, with Python API
all BOSS functionalities can be executed within Python.

To get started with the BOSS binary, see the following pages for CLI
and API:

* Quickstart
  ( :ref:`CLI<quickstart_cli>` | :ref:`API<quickstart_py>` )
* Post-processing
  ( :ref:`CLI<postprocessing_cli>` | :ref:`API<postprocessing_py>` )
* Restarting
  ( :ref:`CLI<restarting_cli>` | :ref:`API<restarting_py>` )
* Results
  ( :ref:`API<results_py>` )



A set of more in-depth tutorials are also available for download:

* **Tutorial 1:** Introduction to BOSS input files, user function, and postprocessing
  ( :download:`CLI<tutorials/cli/tutorial_1.zip>` | :download:`API<***>` )

* **Tutorial 2:** Alanine conformer search in 2D and 4D, using the `AMBER <https://ambermd.org/>`_ code 
  ( :download:`CLI<tutorials/cli/tutorial_2.zip>` | :download:`API<***>` )

* **Tutorial 3:** Using existing data to find minima and illustrate landscapes
  ( :download:`CLI<tutorials/cli/tutorial_3.zip>` | :download:`API<***>` )



Advanced features
+++++++++++++++++

BOSS also supports a number of more advanced features. The following
tutorials are for the API, but the commands are the same for CLI:

.. toctree::
   :maxdepth: 4

   tutorials/batches_py
   tutorials/costs_py
   tutorials/symmetry_py
   tutorials/gradient_observations_py
   tutorials/multi_fidelity_py
   tutorials/heteroscedastic_py



Further reading
+++++++++++++++
If you wish to learn more about Gaussian processes and Bayesian optimization, we recommend the following resources:

* `Visual/Interactive introduction to Gaussian Processes <https://distill.pub/2019/visual-exploration-gaussian-processes/>`_
* `Visual/Interactive introduction to Bayesian optimization <https://distill.pub/2020/bayesian-optimization/>`_
* `C. E. Rasmussen and C. K. I. Williams, Gaussian Processes for Machine Learning, MIT Press (2006) <http://www.gaussianprocess.org/gpml/chapters/RW.pdf>`_.
* `E. Brochu et al., A Tutorial on Bayesian Optimization of Expensive Cost Functions, with Application to Active User Modeling and Hierarchical Reinforcement Learning (2010) <https://arxiv.org/pdf/1012.2599.pdf>`_.
* `R. Garnett, Bayesian Optimization, Cambridge University Press (2022) <https://bayesoptbook.com/>`_.
* `P. I. Frazier and J. Wang, Bayesian optimization for materials design (2015) <https://arxiv.org/pdf/1506.01349.pdf>`_.

