Keywords
++++++++
The behaviour of BOSS can be controlled through a set of global keywords that are valid in
both the Python and command line interfaces. All the possible keywords and their values are described below grouped
by theme in the following subsections. In the command line interface, keywords are supplied via the input file as line-separated
``keyword`` ``value`` pairs. Any text preceded by a Hashtag ``#`` in the input file is interpreted as a comment and will
be ignored by BOSS.

File Handling
-------------

| **keyword: choices (default)**
| **description**

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``userfn``\ : filepath [name] (**No default**)
| A relative or absolute path to a python script containing a function (called *name*, or *f* if no name is given), that returns the
  objective value :math:`f(\mathbf{x})` given a variable array :math:`\mathbf{x}`.

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``outfile``\ : filepath (boss.out)
| Filepath to the main output file which contains a summary of the optimization.

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``rstfile``\ : filepath (boss.rst)
| Filepath to the restart file which contains the current keywords as well as acquired
  data and model hyperparameters. Can be used as an input file for
  restarting a previous optimization and/or doing post-processing.

Main Options
------------

| **keyword: choices (default)**
| **description**

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``bounds``\ : min1 max1; min2 max2; ... minN maxN (**No default**)
| Bounds for the variables defining the domain/space of the user
  function (see ``userfn``). The user function will only be passed arrays
  :math:`\mathbf{x}` that belong to this domain/space.

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``kernel``\ : rbf, stdp, mat32 or mat52 (rbf)
| Which kernel (i.e. covariance function) to use. If just one keyword is
  specified, that kernel will be used for all variables. Mixing kernels
  for N different variables (by multiplication) is done by specifying
  N kernels. For instance, a 3D problem where the last variable is not
  periodic could be specified with stdp stdp rbf.

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``periods``\ : per1 per2 ... perN (bounds[max] - bounds[min] for each variable)
| The periodicity of each variable, required by the stdp kernel.
  Defaults to the interval length of the individual variable bounds.

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``yrange``\ : y_min y_max (None)
| An estimated range for the objective function, i.e. lower and upper limits that objective values :math:`f(\mathbf{x})`
  are likely to fall within for any given :math:`\mathbf{x}` in the domain. If not specified, a range will be estimated 
  based on objective function values calculated for the initial data, e.g., Sobol sequence.

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``ynorm``\ : 1 or 0 (0)
| Turns normalization of y-data on or off.


.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``noise``\ : non-negative float (:raw-html:`10<sup>-12</sup>`)
| Estimated variance for the (Gaussian) noise in the objective function :math:`f(\mathbf{x})` evaluations.

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``inittype``\ : sobol, random or grid (sobol)
| The method for creating the initial sampling locations
  :math:`{\bf x}`. If the input file is a restart file, any data points
  supplied therein will override this setting.

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``initpts``\ : positive integer (5)
| The number of initial data points to create/read. If the input file is
  a restart file, data points supplied therein will override this setting.

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``iterpts``\ : positive integer (:raw-html:`int(15*dim<sup>1.5</sup>)`)
| The number of Bayesian optimization iterations to run.

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``glmin_tol``\ : dxtol niters (None)
| Turns on a convergence check where the BO stops if the change in the global minimum prediction
  location :math:`x_{glmin}` has not exceeded ``dxtol`` in the last ``niters`` BO iterations (even if the 
  the current number of iterations is less than ``iterpts``). The default value of None means that no convergence check is performed.


Data Acquisitions
-----------------

| **keyword: choices (default)**
| **description**


.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``acqfn_name``\ : elcb, lcb, ei, explore or exploit (elcb)
| The choice of the acquisition function for selecting the next sampling
  location(s) during the BO process. Note that if the predictive variance at the acquisition location 
  is smaller than ``acqtol``, that acquisition is discarded and a new location is sampled using pure exploration.
  The acquisition function names can be extended with multi (for example: elcb_multi) to create a multi-task acquisition heuristic.

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``acqfnpars``\ : par1, par2, ... (No default)
| Additional arguments to the acquisition function. Currently only used
  (optionally) by LCB to give a tradeoff parameter.

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``acqtol``\ : float or None (0.001)
| The tolerance for how small of a model variance is allowed in the next
  suggested acquisition location :math:`x_{next}` before triggering pure
  exploration.

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``batchtype``\ : sequential, kb (sequential)
| The type of batch acquisition strategy to use. Currently, the only implemented strategies are
  kb for Kriging-Believer-style batches and sequential for regular, non-batched, acquisitions.

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``batchpts``\ : positive integer (1)
| The number of acquisitions in each batch. Only applicable if ``batchtype`` is not sequential.


GP Hyperparameters
------------------

| **keyword: choices (default)**
| **description**

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``thetainit``: var ls1 ls2 .. lsN
| (var :math:`\Delta`\yrange/2 or 1/2 if ``ynorm``\=1; ls :math:`\pi`/10 for periodic,
  (bounds[max] - bounds[min])/20 for non-periodic
  variables)
| Initial value of kernel variance, followed by initial values for the
  kernel lengthscales.

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``thetabounds``\ : minv maxv; min1 max1; min2 max2; ... minN maxN
| (None)
| Hard bounds for kernel variance and lengthscales. Note that, currently, these bounds can only be
  approximately applied as long as hyperparameter priors are used.

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``thetaprior``\ : gamma or None (gamma)
| Prior distribution to use for the hyperparameters. If set to none, no
  informative prior is used. Gamma distribution takes two parameters:
  shape and rate. (See ``thetapriorpar``)

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``thetapriorpar``\ :
  par1_v par2_v; par1_1 par2_1; par1_2 par2_2; ... par1_N par2_N
| (defaults based on ``yrange`` and ``kernel``)
| Parameters of the prior for kernel variance and lengthscales.
  Parameters required depend on the prior (see ``thetaprior``).


Hyperparameter Optimization
---------------------------

| **keyword: choices (default)**
| **description**

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``optimtype``\ : score or multi_cg (score)
| Which algorithm to use for hyperparameter optimization and minimization
  of the surrogate model. The default option *score* uses a vectorized random scoring
  of candidate solutions followed by a single conjugate gradient refinement. This
  is a fast and accurate approach for most problems. The other option *multi_cg* launches
  a number of conjugate gradient optimizers from a set of chosen starting points. This
  is typically slower than the scoring-based approach and was the default algorithm
  in older BOSS versions.

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``initupdate``\ : 1 or 0 (1)
| Whether to optimize the kernel hyperparameters immediately
  initializing the first GP model containing the initial data.

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``updatefreq``\ : positive integer (1)
| The frequency (in terms of BO iterations) of optimizing the
  hyperparameters by maximizing the marginal likelihood.

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``updateoffset``\ : non-negative integer (0)
| The BO iteration from which to start optimizing kernel hyperparameters
  in intervals (see ``updatefreq``).

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``updaterestarts``\ : positive integer (2)
| How many random restarts to do in the kernel hyperparameter’s space
  when optimzing their values (i.e. maximizing marginal likelihood).

.. _keywords_pp:


Post-processing
---------------

| **keyword: choices (default)**
| **description**


.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

.. _sec_pp_iters:
| ``pp_iters``\ : ``it1 it2 it3 ... itN`` (``None``: use all available iterations)
| The iterations to apply all post-processing functionality to. The 0th
  iteration refers to the first fit of GP model including just the
  initial data. If post-processing is called and *pp_iters* has not been
  specified all available iterations will be used.

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

.. _sec_pp_models:
| ``pp_models``\ : 1 or 0 (0)
| Whether to output and plot GP models and uncertainty. See also
  ``pp_model_slice``.

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

.. _sec_pp_acq_funcs:
| ``pp_acq_funcs``\ : 1 or 0 (0)
| Whether to output and plot acquisition functions or slices of them in
  a grid according to ``pp_model_slice``.

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

.. _sec_pp_model_slice:
| ``pp_model_slice``\ : ``var1 var2 npts`` (1 1 50 in 1D; 1 2 25
  in 2D+)
| Which (max 2D) cross-section of the objective function domain to use
  in output and plots. First two integers define the cross-section and
  last determines how many points per edge in the dumped grid. If first
  two are the same, it means a 1D cross-section.

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

.. _sec_pp_var_defaults:
| ``pp_var_defaults``\ : ``def1 def2 ... defN`` (``None``)
| Which default values to fix each variable to if they are not included
  in the model and true function output and plots. If this keyword is
  ``None`` the :math:`x_{glmin}` values for each variable are used as a
  default. For acquisition function plots the same applies, but default
  values are the :math:`x_{next}` values if they are available.

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

.. _sec_pp_truef_npts:
| ``pp_truef_npts``\ : positive integer (``None``)
| Determines how big of a grid to use for outputting the true/objective
  function by making extra calls to user function. The grid size is
  defined as points per edge. Leaving this keyword to its default None
  will result in no true function dump.

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

.. _sec_pp_local_minima:
| ``pp_local_minima``\ : float between 0.0 and 1.0 (``None``)
| Determines the proportion (:math:`x \cdot 100\%`) of the lowest
  acquisitions to use for starting minimizers in an attempt to extract
  all the local minima of the GP model. Leaving this keyword to its
  default None means no local minima search.

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

.. _sec_pp_truef_at_glmins:
| ``pp_truef_at_glmins``\ : 1 or 0 (0)
| Whether to calculate true function values at model minimum prediction
  locations :math:`x_{glmin}`. Note that this causes extra calls
  to the user function.

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

.. _sec_mep_rrtsteps:
| ``mep_rrtsteps``\ : positive integer (10000)
| The maximum number of steps that the energy threshold is increased and
  the RRT trees grown. Defines the energy threshold step
  :math:`\Delta e` by dividing the energy range.

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">


.. _sec_mep_maxe:
| ``mep_maxe``\ : float (defaults to maximum observed energy)
| The high end of the energy range that is considered. Must be higher
  than the maximum energy of all minimum energy paths. (The low end is
  always taken as the energy of the second lowest local minimum.)

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

.. _sec_mep_precision:
| ``mep_precision``\ : positive integer (25)
| The precision of collision detection and density of NEB points. Is
  defined as the number of points that would be considered by a path
  directly along the shortest dimension.

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

.. _sec_mep_nebsteps:
| ``mep_nebsteps``\ : integer, at least 2 (20)
| The maximum number of iterations for optimizing the obtained paths
  with the nudged elastic band method, before returning the final
  result.


Convergence
-----------

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``convtype``\ : glmin_val, glmin_loc (None)
| Selects and activates a convergence checker. With the glmin_val option, the checker will monitor the change in the predicted global minimum :math:`\mu_{\text{glmin}}` while the glmin_loc opti  on will monitor the change in :math:`\mathbf{x}_{\text{glmin}}`, see :ref:`definitions <BOSS definitions>` for more details. 
  The default ``convtype`` value of None means that no convergence check is performed.
  For tuning the sensitivity of the convergence see ``conv_abstol``, ``conv_reltol``, and ``conv_window``. 

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``conv_reltol``\ : positive float (None)
| Relative tolerance for the convergence checker. See :ref:`definitions <BOSS definitions>`.

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``conv_abstol``\ : positive float (:raw-html:`10<sup>-7</sup>`)
| Absolute tolerance for the convergence checker. See :ref:`definitions <BOSS definitions>`.

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``conv_iters``\ : positive integer (5)
| An iteration number to act as a window for the convergence checker. That is, if the convergence check is successful for the number of iterations
  specificed by the window the BO loop will terminate.

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``maxcost``\ : float or None (None)
| Enables a cost limit check where BO stops if the next acquisition would make the cumulative acquisition costs exceed ``maxcost``. Requires a cost-aware acquisition function.


Miscellaneous
--------------

| **keyword: choices (default)**
| **description**


.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``seed``\ : non-negative integer (None)
| A fixed seed passed to all random generators invoked by BOSS. By specifying a seed the output of BOSS becomes deterministic, which facilitates, .e.g,  debugging and performance tests.


.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``initscramble``\ : True or False (False)
| Scrambles, i.e., adds small random displacements, to the initial points. Currently only applicable to ``inittype=sobol``, in which case the scrambled sequence maintains the Sobol balancing property. Scrambling is useful, e.g., for gathering benchmark statistics since the convergence rate is sensitive the choice of initial points. 


.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

.. _sec_parallel_optims:
| ``parallel_optims``\ : non-negative integer (0)
| Enables parallel optimizers for the legacy ``multi_cg`` optimizer that can be activated via the ``optimtype`` keyword. Should be set to a target number of parallel optimizers to be launched by BOSS during optimization of the acquisition function and the surrogate model (for determining the global minimum). This option can give significant speed-ups on supercomputers but should not be enabled otherwise. We recommend testing with 12 to 24 parallel optimizers and setting the environment variables ``OMP_NUM_THREADS=1``, ``MKL_NUM_THREADS=1``, ``NUMEXPR_NUM_THREADS=1``.

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

.. _sec_minfreq:
| ``minfreq``\ : positive integer (1)
| The frequency with which the surrogate models is minimized to determine the current global minimum. Specifically, the surrogate models is minimized every ``minfreq``-th iteration. Increasing ``minfreq`` will thus lead to fewer minimizations and better performance at the cost of missing global minimum information in the output.

Multi-task
----------

| **keyword: choices (default)**
| **description**


.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``userfn_list``\ : filepath0 [name0]; filepath1 [name1]; ... filepathK [nameK] (No default)
| Relative or absolute paths to the function that we want to optimise (target task) and K related functions (support tasks) that can be queried in optimisation. The function that we want to optimise must be listed first. See also ``userfn``.

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``num_tasks``\ : non-negative integer (None)
| The number of tasks represented in the initialisation and acquisition data. The number of tasks can exceed the number of functions listed in ``userfn_list`` if optimisation is initialised with precomputed data that includes acquisitions from a task that is not available for additional queries. Defaults to number of functions listed in ``userfn_list``.

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``task_initpts``\ : npts0 npts1 ... nptsK (5 5 ... 5)
| The number of initial data points to create with each task listed in ``userfn_list``. If the input file is a restart file, any data points supplied therein will override this setting.

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``task_cost``\ : cost0 cost1 ... costK (1 1 ... 1)
| The acquisition cost associated with each task listed in ``userfn_list``. Multi-task acquisition rules use the acquisition costs to decide which task is queried in optimisation.

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``W_rank``\ : non-negative integer (1)
| Number of columns in the low-rank matrix W that models correlations between tasks.

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``W_init``\ : w0 w1 ... wD (0 0 ... 0 )
| Initial value for the low-rank matrix W that models correlations between between tasks. The hyperparameter W is a matrix with ``num_tasks`` rows and ``W_rank`` columns.

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``W_prior``\ : fixed_value, gaussian, or None (None)
| Prior distribution type to use for all elements in W. If None, no informative prior is used. Fixed value takes one parameter and Gaussian distribution takes two parameters.

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``W_priorpar``\ : par1 [par2] (None)
| Parameters for the prior distribution selected with ``W_prior``. Parameters required depend on the prior (see ``W_prior``).

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``kappa_init``\ : kappa0 kappa1 ... kappaK (default based on ``thetainit``)
| Initial value for the vector kappa that adds independent variation to the task covariance matrix. The hyperparameter kappa is a vector with ``num_tasks`` elements.

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``kappa_prior``\ : fixed_value, gamma, or None (None)
| Prior distribution type to use for all elements in kappa. If None, no informative prior is used. Fixed value takes one parameter and Gamma distribution takes two parameters.

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``kappa_priorpar``\ : par1 [par2] (None)
| Parameters for the prior distribution selected with ``kappa_prior``. Parameters required depend on the prior (see ``kappa_prior``).
