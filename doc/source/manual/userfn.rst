User function
+++++++++++++

The user function serves as an interface between BOSS and the data
source. The user function supplies data points to the active learning
BO algorithm, in response to acquisition function queries. Depending
on the optimized target property, the data can originate from many
kinds of data sources, for example an analytic function, a simulation
software, or an experimental setup.

The data involves the target function :math:`f(x)` evaluated for a set
of simulation (or experimental) variables :math:`x`. The user function
is a Python script, which evaluates and returns :math:`f(x)`. In case
of simulations, the user function includes a command to execute the
simulation with given input parameters :math:`x`. In experiments, the
user function can ask the user to perform the experiment using
parameters :math:`x`. The script then waits for the user to input the
evaluated value :math:`f(x)` before proceeding to the next iteration.

.. figure:: ../figures/user_fn2.png
   :width: 100.0%

   Fig. 1: Tasks performed by the BO algorithm (green) and the user
   function (purple), which exchanges information between BOSS and the
   data source (simulation or experiment, orange).

Fig. 1 illustrates how the user function script is employed. At each
iteration, the script receives the feature vector :math:`x` and
returns :math:`f(x)`. In the following, we present two example cases
to illustrate how the user function is employed in practice.


.. raw:: html

   <h2>Degrees of freedom</h2>

.. _sec_dof:

In a BOSS search, the dimensions of the phase space are determined by
the number of parameters considered, i.e. degrees of freedom (DoF) in
the problem. In theory, this is often a very large number. In
practice, there are usually several parameters that critically
determine the target property. These should be selected as DoF to keep
the number of search dimensions tractable. Using physical variables as
DoF will make the BOSS surrogate models physically interpretable. A
range of choices successfully employed in BOSS to date (see `research
<https://sites.utu.fi/boss/research/>`_) include:


* Torsional angles: for molecular conformer search, or flexible
  molecules (see **Tutorial 2** in the :ref:`Tutorials<Tutorials>`
  section)

* Molecular orientation, registry :math:`(x-y)`, and height
  :math:`(z)` above the surface: for molecular surface adsorption
  studies (see **Tutorial 3** in the :ref:`Tutorials<Tutorials>`
  section and :ref:`Example 1<userfn_example_1>` below)


* AI model hyperparameters: for simultaneous optimization of
  hyperparameters of materials descriptors and machine learning models
  (for example `kernel ridge regression <https://iopscience.iop.org/article/10.1088/2632-2153/abee59/meta>`_)
  
* Theoretical model parameters: for optimizing theoretical models to
  fit experimental data

* Experimental processing conditions: for optimizing the outcomes of
  experiments, for example the `yield in a chemical process <https://pubs.acs.org/doi/full/10.1021/acssuschemeng.2c01895>`_.


.. .. figure:: ../figures/fig_atmo.png :width: 70.0%

   Fig. 2: BOSS application to surface adsorption of a molecular
   conformer: orientational degrees of freedom.

.. Success of BOSS simulations is guaranteed by a good choice of DoF
  variables, system parameters that critically determine the target
  property. The stability of molecular conformers typically depends on the
  torsional angles. Molecule-surface interactions (see Fig `4 <#dof>`__)
  depend on molecular orientation and registry (x-y) location, while
  molecule-surface distance typically introduces a constant shift to the
  interaction. Performance of machine learning models depends on a handful
  of key parameters. Accuracy of DFT simulations depends on several
  principal settings. Many properties can be tuned with BOSS, as long as
  the relevant DoFs are correctly identified.

Surrogate model complexity depends on the dimensionality (DoF), number
of local minima and the spread of their values. BOSS has been tested
up to 10D with few minima, or up to 6D with many minima. The number of
data acquisitions needed for meaningful results increases with search
complexity, approximately up to 100 data for 1--3D, and up to 500 for
3--6D.  Convergence behaviour depends on problem details, and on
kernel choice: periodic kernels scale considerably better with
dimensions than non-periodic ones.

.. raw:: html

    <h2>Examples</h2>

.. toctree::
   :maxdepth: 4

   userfn_example_1
   userfn_example_2
