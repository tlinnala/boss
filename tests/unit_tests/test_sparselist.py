import numpy as np
import pytest

from boss.utils.sparselist import SparseList, wrap_index


def test_wrap_index():
    assert wrap_index(3, 5) == 3
    assert wrap_index(-1, 5) == 4
    assert wrap_index(6, 5) == 6
    assert wrap_index(0, 5) == 0
    assert wrap_index(-5, 5) == 0


@pytest.fixture
def splist_empty():
    return SparseList()


@pytest.fixture
def splist_one():
    splist = SparseList()
    splist.data = {0: 1}
    splist._inds_values = [0]
    splist.ind_max = 0 
    return splist


@pytest.fixture
def splist():
    splist = SparseList(default=np.nan)
    splist.data[0] = 0
    splist.data[4] = 125
    splist.data[10] = 40
    splist.data[11] = 2
    splist._inds_values = [0, 4, 10, 11]
    splist.ind_max = 11
    return splist

@pytest.fixture
def splist2():
    splist = SparseList()
    splist.data[0] = np.array([0., 1.])
    splist.data[2] = np.array([125, 3.])
    splist.data[5] = np.array([3., 7.])
    splist._inds_values = [0, 2, 5]
    splist.ind_max = 5
    return splist2


def test_len(splist_empty, splist_one, splist):
    assert len(splist_empty) == 0
    assert len(splist_one) == 1
    assert len(splist) == 12


def test_setitem(splist_empty, splist_one, splist):
    with pytest.raises(ZeroDivisionError):
        splist_empty[-1] = 0

    # tests assignment
    for d in [splist_empty, splist_one]:
        d[7] = 1
        assert d.data[7] == 1
        d[0] = 2  
        assert d.data[0] == 2
        assert d.ind_max == 7

        # tests index wrapping
        d[-1] = 3
        assert d.data[7] == 3
        d[-8] = 0
        assert d.data[0] == 0

    # tests prefilled splist
    splist[11] = 77
    assert splist.data[11] == 77
    splist[-1] = 12
    assert splist.data[len(splist) - 1] == 12
    splist[-len(splist)] = 33
    assert splist.data[0] == 33


def test_init():
    data = {0: 3, 4: 125, 10: 40, 11: 2}
    splist = SparseList(data=data, default=0)
    assert splist.data == data
    assert splist.ind_max == 11
    assert splist[1] == 0


def test_eq(splist):
    data = {0: 0, 4: 125, 10: 40, 11: 2}
    splist_dupl= SparseList(data=data, default=0)
    assert splist == splist_dupl


def test_from_array(splist_empty, splist):
    splist_empty_dupl = SparseList.from_array([])
    assert splist_empty == splist_empty_dupl

    arr = np.array([splist.data.get(i) for i in range(len(splist))])
    splist_dupl = SparseList.from_array(arr)
    assert splist == splist_dupl


def test_copy(splist):
    splist_copy= splist.copy()
    assert (
        splist.data == splist_copy.data
        and splist.ind_max == splist_copy.ind_max
        and splist._inds_values == splist_copy._inds_values
    )


def test_bool(splist_empty, splist):
    assert splist
    assert not splist_empty


def test_getitem(splist_empty, splist_one, splist):
    # tests index wrapping and access for empty splist
    with pytest.raises(IndexError):
        v = splist_empty[0]
        w = splist_empty[-1]

    # same for 1 element splist
    assert splist_one[0] == 1
    assert splist_one[-1] == 1
    with pytest.raises(IndexError):
        v = splist_one[-len(splist_one) - 1]

    # same for prefilled splist
    assert splist[0] == splist[-len(splist)]
    assert splist[-1] == splist[len(splist) - 1]
    with pytest.raises(IndexError):
        v = splist[-len(splist) - 1]
        v = splist[len(splist)]


def test_inds_values(splist_empty, splist):
    with pytest.raises(ZeroDivisionError):
        splist_empty.from_value_index(0)

    assert splist.from_value_index(0) == 0
    assert splist.from_value_index(3) == 11


def test_to_value_index(splist_empty, splist):
    with pytest.raises(IndexError):
        splist_empty.to_value_index(0)
        splist.to_value_index(len(splist) + 1)
        splist.to_value_index(-len(splist) - 1)

    assert splist.to_value_index(0) == 0
    assert splist.to_value_index(11) == 3
    assert splist.to_value_index(-1) == 3


def test_append(splist_empty, splist):
    splist_empty.append(15)
    assert splist_empty.data[0] == 15
    assert splist_empty.ind_max == 0
    assert splist_empty._inds_values == [0]

    splist.append(-1)
    assert splist.data[12] == -1
    assert splist.ind_max == 12
    assert splist._inds_values == [0, 4, 10, 11, 12]


def test_value(splist_empty, splist):
    with pytest.raises(ZeroDivisionError):
        splist_empty.value(0)
    
    assert splist.value(-1) == splist.value(len(splist.data) - 1) == 2
    assert splist.value(0) == splist.value(-len(splist.data)) == 0
    with pytest.raises(IndexError):
        splist.value(-len(splist.data) - 1)
        splist.value(len(splist.data))


def test_to_array(splist_empty, splist_one, splist):
    assert np.array_equal(splist_empty.to_array(), np.array([]))
    assert np.array_equal(splist_one.to_array(), np.array([1]))
    splist_array = np.array(list(splist.data.values()))
    assert np.array_equal(splist.to_array(), splist_array)


def test_is_default(splist_empty, splist):
    with pytest.raises(IndexError):
        splist_empty.is_default(0)
    assert not splist.is_default(4)
    assert splist.is_default(3)
    splist[4] = np.nan
    assert splist.is_default(4)
    splist[4] = None
    assert not splist.is_default(4)
    splist.default = None
    assert splist.is_default(4)
