import pytest
from pathlib import Path
from pytest import approx

import numpy as np

from boss.bo.bo_main import BOMain
from boss.bo.initmanager import InitManager
from boss.bo.model import STModel
from boss.utils.os import remove_boss_files


class CountedTestFunc:
    def __init__(self):
        self.called = 0

    def __call__(self, X):
        self.called += 1
        return np.squeeze(np.sum(np.atleast_2d(X)**2, axis=1))

    def reset(self):
        self.called = 0


def get_sobol_seq(bounds, num_pts):
    im = InitManager('sobol', bounds, num_pts)
    X_sobol = im.get_all()
    return X_sobol


@pytest.fixture()
def bo_minimal():
    f = CountedTestFunc()
    bo = BOMain(
        f,
        bounds=[0., 1.],
        initpts=5,
        iterpts=2,
        seed=123,
        yrange=[-10, 10],
        kernel='rbf'
    )
    yield bo
    remove_boss_files()


def test_init_model(bo_minimal):
    bo = bo_minimal
    f = CountedTestFunc()
    X = np.random.rand(bo.settings['initpts'], 1)
    Y = np.atleast_2d(f(X)).T
    bo.init_model(X, Y)
    assert isinstance(bo.model, STModel)


def test_get_initpts(bo_minimal):
    bo = bo_minimal
    initpts = bo.settings['initpts']

    # nothing given
    X_init, _ = bo.get_initpts()
    X_sobol = get_sobol_seq(bo.settings['bounds'], initpts)
    assert X_init == approx(X_sobol)

    # initpts on rstmanager
    bo.rst_manager.data = np.random.rand(initpts, 4)
    X_init, Y_init = bo.get_initpts()
    assert X_init == approx(bo.rst_manager.X)
    assert Y_init == approx(bo.rst_manager.Y)


def test_init_run(bo_minimal):

    def assert_is_init(bo, X_init, Y_init):
        isinstance(bo.model, STModel)
        assert (Path.cwd() / 'boss.rst').is_file()
        assert (Path.cwd() / 'boss.out').is_file()
        assert bo.model.X == approx(X_init)
        assert bo.model.Y == approx(Y_init)

    bo = bo_minimal

    f = CountedTestFunc()
    X_init = get_sobol_seq(bo.settings['bounds'], bo.settings['initpts'])
    Y_init = np.atleast_2d(f(X_init)[:, None])

    # with only X points
    bo.init_run(X_init)
    assert_is_init(bo, X_init, Y_init)
    assert bo.user_func.func.called == X_init.shape[0]
    bo.user_func.func.reset()

    # Test with with X and full Y points, in this case the userfn
    # should have 0 calls
    bo = BOMain.from_settings(bo.settings)
    Y_init = np.atleast_2d(f(X_init)[:, None])
    bo.init_run(X_init, Y_init)
    assert_is_init(bo, X_init, Y_init)
    assert bo.user_func.func.called == 0
    bo.user_func.func.reset()

    # Test with X and partial Y points:
    # nbr of evals should be == nbr of Y points
    bo = BOMain.from_settings(bo.settings)
    Y_init2 = np.atleast_2d(f(X_init[:-2])[:, None])
    bo.init_run(X_init, Y_init2)
    assert_is_init(bo, X_init, Y_init)
    assert bo.user_func.func.called == (X_init.shape[0] - Y_init2.shape[0])


def test_run(bo_minimal):
    bo = bo_minimal
    res = bo.run()
    assert res.select('mu_glmin', -1) == approx(0, abs=1e-6)


def test_update_model(bo_minimal):
    pass


def test_evaluate_user_func(bo_minimal):
    pass


def test_acquire(bo_minimal):
    bo = bo_minimal
    X_init, Y_init = bo.get_initpts()
    bo.init_run(X_init, Y_init)
    X_next = bo.acquire()
    assert isinstance(X_next, np.ndarray)
