import unittest
from unittest.mock import Mock
import pytest

import numpy as np
from numpy.testing import assert_allclose, assert_equal
from scipy.spatial.distance import euclidean
import boss.bo.kernel_factory as kernel_factory
from boss.bo.model import STModel, GradientModel
from boss.settings import Settings


class ModelTest(unittest.TestCase):
    """
    Test cases for Model class
    """

    def setUp(self):
        """
        Initialize instance of Model class for each test
        """
        keywords = {}
        bounds = np.array([[0, 5], [0, 5], [0, 5]])
        self.dim = len(bounds)
        keywords["bounds"] = bounds
        keywords["kernel"] = ["rbf", "stdp", "stdp"]
        keywords["thetainit"] = [4, 3, 2, 1]
        keywords["periods"] = [5, 5, 5]
        keywords["thetabounds"] = [(0, 10)] * (self.dim + 1)
        keywords["thetaprior"] = "gamma"
        keywords["thetapriorpar"] = [[1, 1]] * (self.dim + 1)
        settings = Settings(keywords)

        self.kerntype = settings["kernel"]
        self.bounds = settings["bounds"]
        self.periods = settings["periods"]
        self.thetainit = settings["thetainit"]
        
        x0 = np.array([[0, 0, 0], [2, 2, 2], [4, 4, 4]])
        y0 = np.array([[0], [-1], [0]])
        settings['yrange'] = [np.min(y0), np.max(y0)]
        self.kernel = kernel_factory.select_kernel(settings)
        self.noise = 1e-10
        self.ynorm = False

        self.model = STModel(
            self.kernel,
            x0,
            y0,
            self.noise,
            self.ynorm,
        )

    def test_add_data(self):

        xnew = np.array([1, 1, 1])
        ynew = np.array([0.5])

        self.model.add_data(xnew, ynew)
        assert_equal(self.model.Y[-1], ynew)
        assert_equal(self.model.X[-1], xnew)

    def test_redefine_data(self):

        xnew = np.array([[1, 1, 1], [2, 2, 2], [3, 3, 3]])
        ynew = np.array([[0.5], [1.5], [2.5]])

        self.model.redefine_data(xnew, ynew)

        assert_equal(self.model.Y, ynew)
        assert_equal(self.model.X, xnew)

    def test_set_get_unfixed_params(self):

        params_in = [1, 2, 3, 4]

        self.model.set_unfixed_params(params_in)
        params_out = self.model.get_unfixed_params()
        assert_equal(params_in, params_out)

    def test_get_all_params(self):

        pars = self.model.get_all_params()

        self.assertEqual(pars['variance'], self.thetainit[0])
        assert_equal(pars['lengthscales'], self.thetainit[1:])
        assert_equal(pars['periods'], self.periods[1:]) 
                     

    def test_mu(self):

        mu = self.model.predict(self.model.X)[0]

        assert_allclose(mu, self.model.Y, atol=np.sqrt(self.noise))

    # def test_nu(self):

    def test_predict_mean_grad(self):

        x0 = np.array([[0, 0, 0], [2, 2, 2], [4, 4, 4]])
        y0 = np.array([[0], [0], [0]])

        model = STModel(
            self.kernel,
            x0,
            y0,
            self.noise,
            self.ynorm,
        )

        m, dmdx = model.predict_mean_grad(model.X, norm=False)

        assert_allclose(m, model.Y, atol=np.sqrt(self.noise))
        assert_allclose(dmdx, np.zeros(dmdx.shape), atol=np.sqrt(self.noise))

    def test_get_best_xy(self):

        xb = np.array([2, 2, 2])
        yb = np.array([-1])

        x, y = self.model.get_best_xy()

        assert_equal(x, xb)
        assert_equal(y, yb)

    def test_predict_mean_sd_grads(self):

        x0 = np.array([[0, 0, 0], [2, 2, 2], [4, 4, 4]])
        y0 = np.array([[0], [0], [0]])
        model = STModel(
            self.kernel,
            x0,
            y0,
            self.noise,
            self.ynorm,
        )

        m, s, dmdx, dsdx = model.predict_mean_sd_grads(model.X, norm=False)

        for i in range(len(m)):
            with self.subTest(i=i):
                self.assertAlmostEqual(m[i, 0], model.Y[i, 0], delta=s[i, 0])
                self.assertAlmostEqual(
                    euclidean(dmdx[i], np.zeros(dmdx[i].shape)),
                    0,
                    delta=np.linalg.norm(dsdx[i]),
                )


@pytest.mark.ygrad
class GradientModelTest(unittest.TestCase):
    """
    Test cases for GradientModel class
    """

    def setUp(self):
        """
        Initialize instance of GradientModel class for each test
        """

        # keywords = {"bounds": np.array([[0, 5], [0, 5], [0, 5]])}
        # settings = Settings(keywords)
        # self.bounds = settings["bounds"]
        # self.dim = settings.dim
        # settings["kernel"] = self.kerntype = ["rbf", "stdp", "stdp"]
        # self.thetainit = [4, 3, 2, 1]
        # self.periods = [None, 5, 5]
        # settings["periods"] = self.periods
        # settings["thetainit"] = self.thetainit
        # settings["thetabounds"] = [(0, 10)] * (self.dim + 1)
        # settings["thetaprior"] = "gamma"
        # settings["thetapriorpar"] = [[1, 1]] * (self.dim + 1)
        # self.kernel = kernel_factory.select_kernel(settings)

        keywords = {}
        bounds = np.array([[0, 5], [0, 5], [0, 5]])
        self.dim = len(bounds)
        keywords["bounds"] = bounds
        keywords["kernel"] = ["rbf", "stdp", "stdp"]
        keywords["thetainit"] = [4, 3, 2, 1]
        keywords["periods"] = [5, 5, 5]
        keywords["thetabounds"] = [(0, 10)] * (self.dim + 1)
        keywords["thetaprior"] = "gamma"
        keywords["thetapriorpar"] = [[1, 1]] * (self.dim + 1)
        settings = Settings(keywords)

        self.kerntype = settings["kernel"]
        self.bounds = settings["bounds"]
        self.periods = settings["periods"]
        self.thetainit = settings["thetainit"]

        x0 = np.array([[0, 0, 0], [2, 2, 2], [4, 4, 4]])
        y0 = np.array([[0, -1, -1, -1], [-1, 0, 0, 0], [0, -2, -2, -2]])
        settings['yrange'] = [np.min(y0[:, 0]), np.max(y0[:, 0])]
        self.kernel = kernel_factory.select_kernel(settings)
        self.noise = 1e-10
        self.ynorm = False

        self.model = GradientModel(
            self.kernel,
            x0,
            y0,
            self.noise,
            self.ynorm,
        )

    def test_add_data(self):

        xnew = np.array([[1, 1, 1]])
        ynew = np.array([[0.5, 0.1, 0.2, 0.3]])

        self.model.add_data(xnew, ynew)
        assert_equal(self.model.Y[-1], np.squeeze(ynew))
        assert_equal(self.model.X[-1], np.squeeze(xnew))

    def test_redefine_data(self):

        xnew = np.array([[1, 1, 1], [2, 2, 2], [3, 3, 3]])
        ynew = np.array([[0.5, 1, 2, 3], [1.5, 1, 2, 3], [2.5, 1, 2, 3]])

        self.model.redefine_data(xnew, ynew)

        assert_equal(self.model.Y, ynew)
        assert_equal(self.model.X, xnew)

    def test_set_get_unfixed_params(self):

        params_in = [1, 2, 3, 4]

        self.model.set_unfixed_params(params_in)
        params_out = self.model.get_unfixed_params()
        assert_equal(params_in, params_out)

    def test_get_all_params(self):

        pars = self.model.get_all_params()

        self.assertEqual(pars['variance'], self.thetainit[0])
        assert_equal(pars['lengthscales'], self.thetainit[1:])
        assert_equal(pars['periods'], self.periods[1:])

    def test_mu(self):

        mu = self.model.predict(self.model.X)[0]

        assert_allclose(mu, self.model.Y[:,:1], atol=np.sqrt(self.noise))

    def test_predict_mean_grad(self):

        x0 = np.array([[0, 0, 0], [2, 2, 2], [4, 4, 4]])
        y0 = np.array([[0, 1, 2, 3], [0, 1, 2, 3], [0, 1, 2, 3]])

        model = GradientModel(
            self.kernel,
            x0,
            y0,
            self.noise,
            self.ynorm,
        )

        m, dmdx = model.predict_mean_grad(model.X, norm=False)

        assert_allclose(m, model.Y[:, :1], atol=np.sqrt(self.noise))
        assert_allclose(dmdx, model.Y[:, 1:, None], atol=np.sqrt(self.noise))

    def test_get_best_xy(self):

        xb = np.array([2, 2, 2])
        yb = np.array([-1])

        x, y = self.model.get_best_xy()

        assert_equal(x, xb)
        assert_equal(y, yb)

    def test_predict_mean_sd_grads(self):

        x0 = np.array([[0, 0, 0], [2, 2, 2], [4, 4, 4]])
        y0 = np.array([[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]])
        model = GradientModel(
            self.kernel,
            x0,
            y0,
            self.noise,
            self.ynorm,
        )

        m, s, dmdx, dsdx = model.predict_mean_sd_grads(model.X, norm=False)

        for i in range(len(m)):
            with self.subTest(i=i):
                self.assertAlmostEqual(m[i, 0], model.Y[i, 0], delta=s[i, 0])
                self.assertAlmostEqual(
                    euclidean(dmdx[i], model.Y[i,1:]),
                    0,
                    delta=np.linalg.norm(dsdx[i]),
                )


if __name__ == "__main__":
    unittest.main()
