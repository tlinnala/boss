from unittest import mock

import numpy as np
import pytest

import boss.bo.convergence as convergence
from boss.bo.results import BOResults


@pytest.fixture
def settings():
    sts = {
        "convtype": "glmin_val",
        "conv_reltol": 1e-3,
        "conv_abstol": 1e-9,
        "conv_iters": 3,
    }
    return sts


@pytest.fixture
def mock_results():
    """Mock a minimal results object to isolate the tests."""
    mu_vals = np.array([6.0, 5.0, 4.91, 4.901, 4.9001, 4.90001, 4.900001])
    mu_glmin = mock.Mock()
    mu_glmin.value = lambda i: mu_vals[i]
    mu_glmin.data = mu_vals

    x_vals = np.hstack((mu_vals[:, None], mu_vals[:, None]))
    x_glmin = mock.Mock()
    x_glmin.value = lambda i: x_vals[i]
    x_glmin.data = mu_vals

    results = mock.MagicMock(spec=BOResults)
    d = {"mu_glmin": mu_glmin, 'x_glmin': x_glmin}
    results.__getitem__.side_effect = d.__getitem__
    return results


def test_select_conv_checker(settings):
    checker = convergence.select_conv_checker(settings)
    assert isinstance(checker, convergence.ConvCheckerVal)
    assert checker.rel_tol == pytest.approx(settings["conv_reltol"])
    assert checker.abs_tol == pytest.approx(settings["conv_abstol"])
    assert checker.n_iters == pytest.approx(settings["conv_iters"])

    settings["convtype"] = "glmin_loc"
    checker = convergence.select_conv_checker(settings)
    assert isinstance(checker, convergence.ConvCheckerLoc)


def test_conv_checker_val(mock_results):
    checker = convergence.ConvCheckerVal(rel_tol=1e-3, abs_tol=1e-9, n_iters=3)

    assert checker.check(mock_results)

    checker.n_iters = 4
    assert not checker.check(mock_results)

    checker.n_iters = 3
    checker.rel_tol = 1e-4
    assert not checker.check(mock_results) 

    # test with known min value
    checker.f_ref = 4.9
    assert checker.check(mock_results)

    checker.n_iters = 6
    assert not checker.check(mock_results)


def test_conv_checker_loc(mock_results):
    checker = convergence.ConvCheckerLoc(rel_tol=1e-3, abs_tol=1e-9, n_iters=3, scale_type='max')

    assert checker.check(mock_results)

    checker.n_iters = 4
    assert not checker.check(mock_results)

    checker.n_iters = 3
    checker.rel_tol = 1e-4
    assert not checker.check(mock_results) 

    # test with known min value
    checker.x_ref = np.array([4.9, 4.9])
    assert checker.check(mock_results)

    checker.n_iters = 6
    assert not checker.check(mock_results)
