import unittest
from itertools import permutations
from unittest.mock import Mock

import numpy as np
from numpy.testing import assert_allclose

from boss.utils.minimization import Minimization as Min


def f(x):
    """
    Branin test function (three global minima)
    """
    if len(x.shape) > 1:
        x1, x2 = x[:, 0], x[:, 1]
    else:
        x1, x2 = x
    b, c, r, s, t = 5.1 / (4 * np.pi ** 2), 5.0 / np.pi, 6, 10, 1.0 / (8 * np.pi)
    val = (x2 - b * x1 ** 2 + c * x1 - r) ** 2 + s * (1 - t) * np.cos(x1) + s
    grad = np.array(
        [
            2 * (x2 - b * x1 ** 2 + c * x1 - r) * (c - 2 * b * x1)
            - s * (1 - t) * np.sin(x1),
            2 * (x2 - b * x1 ** 2 + c * x1 - r),
        ]
    )
    return val, grad


class MinimizationTest(unittest.TestCase):
    """
    Test cases for minimization module functions
    """

    def setUp(self):
        """
        Set bounds and reference solutions
        """
        self.b = np.array([[-5.0, 10.0], [0.0, 15.0]])

        self.y = np.array([0.397887] * 3)
        x1 = np.array([-np.pi, 12.275])
        x2 = np.array([np.pi, 2.275])
        x3 = np.array([9.42478, 2.475])
        self.xrefs = np.array(list(permutations(np.array([x1, x2, x3]))))

    def test_minimize_from_sobol(self):
        num_pts = 20
        res = Min.minimize_from_sobol(f, bounds=self.b, num_pts=num_pts)
        y = res[1]
        x = np.array(res[0])

        # expect: returned function values at solution points are correct
        assert_allclose(y, self.y, rtol=1e-6)

        # expect: solution point is one of the global minima
        xs_found = [np.all(np.isclose(x, xref[0], atol=1e-4)) for xref in self.xrefs]
        self.assertTrue(np.any(xs_found))

    def test_minimize_from_random(self):
        num_pts = 20
        res = Min.minimize_from_random(f, self.b, num_pts)
        y = res[1]
        x = np.array(res[0])

        # expect: returned function values at solution points are correct
        assert_allclose(y, self.y, rtol=1e-6)

        # expect: solution point is one of the global minima
        xs_found = [np.all(np.isclose(x, xref[0], atol=1e-4)) for xref in self.xrefs]
        self.assertTrue(np.any(xs_found))

    def test_minimization_from_score(self):
        res = Min.minimize_using_score(f, self.b)
        y = res[1]
        x = np.array(res[0])

        # expect: returned function values at solution points are correct
        assert_allclose(y, self.y, rtol=1e-6)

        # expect: solution point is one of the global minima
        xs_found = [np.all(np.isclose(x, xref[0], atol=1e-4))
                    for xref in self.xrefs]
        self.assertTrue(np.any(xs_found))

    def test_minimize(self):
        kerntype = ["rbf"] * 2
        pts = np.array(
            [[-3, 12, 0], [-2, 14, 0], [3, 2, 0], [2, 3, 0], [9, 2, 0], [10, 3, 0]]
        )
        x, y = Min.minimize(f, self.b, kerntype, pts, min_dist_acqs=0.1, accuracy=1)

        # expect: returned function value at solution point is correct
        assert_allclose(y, self.y[0], 1e-6)

        # expect: solution point is one of the global minima
        xs_found = [np.all(np.isclose(x, xref[0], atol=1e-4)) for xref in self.xrefs]
        self.assertTrue(np.any(xs_found))


if __name__ == "__main__":
    unittest.main()
