import pytest
from pytest import approx
import numpy as np
import boss.utils.arrays as arrays


def test_shape_consistent_X():
    dim = 1
    x = np.array([1, 2, 3])
    X = arrays.shape_consistent_X(x, dim)
    assert X.shape == (len(x), dim)

    dim = 2
    x = np.array([1, 2, 3])
    with pytest.raises(ValueError):
        X = arrays.shape_consistent_X(x, dim)

    dim = 3
    X = [[1, 2, 3], [4, 5, 6]]
    X_sc = arrays.shape_consistent_X(X, dim)
    assert X == approx(X_sc) 


def test_shape_consistent_XY():
    dim = 1
    x = np.array([1, 2, 3])
    y = np.array([1, 2])
    X, Y = arrays.shape_consistent_XY(x, y, dim, nan_pad=True)
    assert X.shape == (len(x), dim)
    assert Y.shape == (len(x), 1)
    assert np.isnan(Y[-1, 0])

    dim = 2
    x = np.array([[1, 2], [4, 5]])
    X, Y = arrays.shape_consistent_XY(x, None, dim, nan_pad=True)
    assert X.shape == (len(x), dim)
    assert Y.shape == (len(x), 1)
    assert np.all(np.isnan(Y))

    dim = 3
    x = [[1, 2, 3], [4, 5, 6]]
    y = [1, 2]
    X, Y = arrays.shape_consistent_XY(x, y, dim)
    assert X.shape == (len(x), dim)
    assert Y.shape == (len(x), 1)
    assert [[y[0]], [y[1]]] == approx(Y)

    # y too long
    dim = 3
    x = [[1, 2, 3], [4, 5, 6]]
    y = [1, 2, 3]
    with pytest.raises(ValueError):
        X, Y = arrays.shape_consistent_XY(x, y, dim)

    # no padding allowed
    dim = 3
    x = [[1, 2, 3], [4, 5, 6]]
    y = [1]
    with pytest.raises(ValueError):
        X, Y = arrays.shape_consistent_XY(x, y, dim, nan_pad=False)

    # gradient observations
    dim = 1
    x = np.array([1, 2, 3, 4])
    y = np.array([[1, 2, 3], [4, 5, 6]])
    X, Y = arrays.shape_consistent_XY(x, y, dim, nan_pad=True, ygrad=True)
    assert X.shape == (len(x), dim)
    assert Y.shape == (len(x), dim + 1)
    assert np.isnan(Y[-1]).all()

    # missing gradient data
    dim = 1
    x = np.array([1, 2, 3])
    y = np.array([1, 2, 3])
    with pytest.raises(ValueError):
        X, Y = arrays.shape_consistent_XY(x, y, dim, ygrad=True)




