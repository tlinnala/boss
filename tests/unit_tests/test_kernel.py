import GPy
import numpy as np
import pytest

import boss.bo.kernel_factory as factory
from boss.settings import Settings


@pytest.fixture
def settings():
    bounds = np.array([[0.0, 1.0]] * 4)
    dim = len(bounds)
    sts = Settings(
        dict(
            bounds=bounds,
            thetainit=[9.0, 2.1, 2.2, 2.3, 2.4],
            thetaprior="gamma",
            thetapriorpar=[[1.0, 2.0]] * (dim + 1),
            kernel=["stdp", "rbf", "mat32", "mat52"],
            thetabounds=[(0, 10)] * (dim + 1),
            periods=[4.0, 1.0, 1.0, 1.0],
            yrange=[0.0, 1.0],
        )
    )
    return sts

@pytest.fixture
def settings_1d():
    bounds = np.array([[0.0, 1.0]])
    dim = len(bounds)
    sts = Settings(
        dict(
            bounds=bounds,
            thetainit=[9.0, 2.1],
            thetaprior="gamma",
            thetapriorpar=[[1.0, 2.0]] * (dim + 1),
            kernel=["stdp"],
            thetabounds=[(0, 10)] * (dim + 1),
            periods=[4.0],
            yrange=[0.0, 1.0],
        )
    )
    return sts

@pytest.fixture
def settings_multi():
    bounds = np.array([[0.0, 1.0]] * 4)
    dim = len(bounds)
    sts = Settings(
        dict(
            bounds=bounds,
            thetainit=[9.0, 2.1, 2.2, 2.3, 2.4],
            thetaprior="gamma",
            thetapriorpar=[[1, 1]] * (dim + 1),
            kernel=["stdp", "rbf", "mat32", "mat52"],
            thetabounds=[(0, 10)] * (dim + 1),
            periods=[4.0, 1.0, 1.0, 1.0],
            num_tasks=2,
            W_rank=1,
            W_init=0.5 * np.ones(2),
            kappa_init=np.ones(2),
        )
    )
    return sts


@pytest.fixture
def settings_hw():
    """Settings appropriate for the 2d himmelvalley test function"""
    sts = Settings(
        dict(
            bounds=[[-5.0, 5.0], [-5.0, 5.0]],
            thetaprior="gamma",
            kernel=["rbf", "rbf"],
            yrange=[0.0, 10.0],
        )
    )
    return sts


def test_select_kernel(settings_1d, settings, settings_multi):
    # test 1d
    kern_1d = factory.init_kernel(settings_1d)
    assert isinstance(kern_1d, GPy.kern.StdPeriodic)
    assert kern_1d.input_dim == 1
    assert kern_1d.active_dims == [0]
    assert kern_1d.variance == pytest.approx(1.0)
    assert kern_1d.lengthscale == pytest.approx(1.0)
    
    # test higher dims
    kern_st = factory.init_kernel(settings)
    kern_mt = factory.init_kernel(settings_multi)
    kern_types = [
        GPy.kern.StdPeriodic,
        GPy.kern.RBF,
        GPy.kern.Matern32,
        GPy.kern.Matern52,
    ]
    for kern in [kern_st, kern_mt]:
        for i, kpart in enumerate(kern.parts[:4]):
            assert isinstance(kpart, kern_types[i])
            assert kpart.input_dim == 1
            assert kpart.active_dims == [i]

            # thetainit values should not be applied yet
            assert kpart.variance == pytest.approx(1.0)
            assert kpart.lengthscale == pytest.approx(1.0)


def test_set_hyper_values(settings, settings_1d):
    # test 1d first
    kern_1d = factory.init_kernel(settings_1d)
    factory._set_hyper_values(kern_1d, settings_1d)
    thetainit = settings_1d["thetainit"]
    assert kern_1d.variance == pytest.approx(thetainit[0])
    assert kern_1d.period == pytest.approx(settings_1d["periods"][0])
    assert kern_1d.lengthscale == pytest.approx(thetainit[1])

    # higher dim
    thetainit = settings["thetainit"]
    kern = factory.init_kernel(settings)
    factory._set_hyper_values(kern, settings)
    # The variance of first kernel part should be equal to first element of thetainit.
    assert kern.parts[0].variance == pytest.approx(thetainit[0])

    # The first kernel part is periodic and should have the period set.
    assert kern.parts[0].period == pytest.approx(settings["periods"][0])

    for i, kpart in enumerate(kern.parts):
        assert kpart.lengthscale == pytest.approx(thetainit[i + 1])


def test_set_hyper_constraints(settings, settings_1d):
    # test 1d first
    kern_1d = factory.init_kernel(settings_1d)
    factory._set_hyper_constraints(kern_1d, settings_1d)
    assert len(kern_1d.parameters) == 3
    assert not kern_1d.variance.is_fixed
    con = [con for con, _ in kern_1d.variance.constraints.items()][0]
    assert con.lower == pytest.approx(0)
    assert con.upper == pytest.approx(10.0)
    assert kern_1d.period.is_fixed
    assert not kern_1d.lengthscale.is_fixed

    # higher dim
    kern = factory.init_kernel(settings)
    factory._set_hyper_constraints(kern, settings)

    # First kernel part is stdp with three params, period should be fixed
    # but variance should be bounded and not fixed.
    kpart = kern.parts[0]
    assert len(kpart.parameters) == 3
    assert not kpart.variance.is_fixed
    # Hyperparameter bounds are enforced by a Logexp distribution, the upper and     
    # lower limits of which should be set according to thetabounds.
    con = [con for con, _ in kpart.variance.constraints.items()][0]
    assert con.lower == pytest.approx(0)
    assert con.upper == pytest.approx(10.0)

    assert kpart.period.is_fixed
    assert not kpart.lengthscale.is_fixed

    # Check remaining kernel parts
    for kpart in kern.parts[1:]:
        # variance should be fixed
        assert kpart.variance.is_fixed
        # lengthscale should be bounded
        assert not kpart.lengthscale.is_fixed
        con = [con for con, _ in kpart.lengthscale.constraints.items()][0]
        assert con.lower == pytest.approx(0)
        assert con.upper == pytest.approx(10.0)


def has_gamma(param) -> bool:
    """Checks whether a GPy/paramz parameter has a gamma prior."""
    distr = [k for k, _ in param.priors.items()][0]
    return isinstance(distr, GPy.priors.Gamma)


def get_gamma_hyperparams(param) -> tuple[float, float]:
    """Returns the hyperparameters of a gamma prior for a GPy/paramz parameter."""
    if not has_gamma(param):
        raise ValueError("Parameter prior is not a gamma distribution.")
    distr = [k for k, _ in param.priors.items()][0]
    return distr.a, distr.b


def test_set_hyper_priors(settings, settings_hw, settings_1d):

    # Test 1d first
    kern_1d = factory.init_kernel(settings_1d)
    factory._set_hyper_priors(kern_1d, settings_1d)
    assert get_gamma_hyperparams(kern_1d.variance) == pytest.approx((1.0, 2.0))
    assert get_gamma_hyperparams(kern_1d.lengthscale) == pytest.approx((1.0, 2.0))

    # Test higher dim with gamma priors with shape, rate = (1, 1) for all params.
    kern = factory.init_kernel(settings)
    factory._set_hyper_priors(kern, settings)
    assert get_gamma_hyperparams(kern.parts[0].variance) == pytest.approx((1.0, 2.0))
    assert get_gamma_hyperparams(kern.parts[0].lengthscale) == pytest.approx((1.0, 2.0))
    for kpart in kern.parts[1:]:
        assert get_gamma_hyperparams(kpart.lengthscale) == pytest.approx((1.0, 2.0))

    # Test with default gamma priors
    kern = factory.init_kernel(settings_hw)
    factory._set_hyper_priors(kern, settings_hw)
    assert np.round(get_gamma_hyperparams(kern.parts[0].variance), 2) == pytest.approx(
        (2.0, 0.08)
    )
    assert np.round(
        get_gamma_hyperparams(kern.parts[0].lengthscale), 2
    ) == pytest.approx((1.57, 0.65))
    assert np.round(
        get_gamma_hyperparams(kern.parts[1].lengthscale), 2
    ) == pytest.approx((1.57, 0.65))


def test_multi(settings_multi):
    kern = factory.init_kernel(settings_multi)
    factory._set_hyper_values(kern, settings_multi)
    factory._apply_coreg_settings(kern, settings_multi)
    lengthscales = settings_multi["thetainit"][1:]
    for i, k in enumerate(kern.parts[:-1]):
        assert k.lengthscale[0] == lengthscales[i]
        assert k.variance[0] == 1.0
    assert np.all(np.squeeze(kern.B.W) == settings_multi["W_init"])
    assert np.all(np.squeeze(kern.B.kappa) == settings_multi["kappa_init"])
