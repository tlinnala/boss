import os
from collections import UserDict

import numpy as np
import pytest
from pytest import approx

import boss.keywords as bkw
from boss.settings import Settings


@pytest.fixture
def settings_minimal():
    """Minimally initialized settings object for testing __init__. """
    sts = Settings.__new__(Settings)
    super(UserDict, sts).__init__()
    sts.data = {}
    sts.costfn = None           # Required to test set_acqfn properly
    return sts


@pytest.fixture
def settings():
    sts = Settings.__new__(Settings)
    super(UserDict, sts).__init__()
    sts.data = {}
    sts.is_rst = False
    sts.dir = os.getcwd()
    sts.acqfn = None
    for cat in bkw.categories.values():
        sts.update(cat)
    sts["bounds"] = np.array([[0.0, 1.0], [0.0, 1.0]])
    return sts


@pytest.fixture(scope="session")
def ipfile(tmp_path_factory):
    """Temporary boss input file for testing. """

    tmp_file = tmp_path_factory.mktemp("scratch") / "boss.in"
    with open(tmp_file, "w") as fd:
        # fd.write("userfn       gl.py\n")
        fd.write("bounds       0.0 3.5; 1.0 3.0\n")
        fd.write("kernel       stdp rbf\n")
        fd.write("#kernel       rbf\n")
        fd.write("\n")
        fd.write("yrange       -5.0 2.0\n")
        fd.write("initpts   10 # comment\n")
        fd.write("iterpts 55 \n")

    return tmp_file


def test_set_independent_defaults(settings_minimal):

    # restore all values to their defaults
    sts = settings_minimal
    sts.set_independent_defaults(only_missing=False)
    for cat in bkw.get_copied_categories():
        for key, val in cat.items():
            np.testing.assert_equal(sts[key], val)

    # only restore None-valued keywords to their default.
    sts.clear()
    sts["iterpts"] = 100
    sts.set_independent_defaults(only_missing=True)
    assert sts["iterpts"] == 100

    for cat in bkw.get_copied_categories():
        for key, val in cat.items():
            if key != "iterpts":
                np.testing.assert_equal(sts[key], val)


def test_set_acqfn(settings_minimal):
    sts = settings_minimal
    sts["acqfnpars"] = np.array([])
    sts["bounds"] = np.array([])
    acqfn_setting_names = {'elcb': 'ELCB', 'lcb': 'LCB', 'explore': 'Explore',
                           'exploit': 'Exploit', 'ei': 'EI',
                           'mes': 'MaxValueEntropySearch'}
    for name in acqfn_setting_names.keys():
        sts.set_acqfn(name)
        assert type(sts.acqfn).__name__ == acqfn_setting_names[name]
        assert callable(sts.acqfn)


def test_correct(settings_minimal):
    sts = settings_minimal
    sts["pp_acq_funcs"] = True
    sts["bounds"] = np.array([[0.0, 1.0], [0.0, 1.0]])
    sts["kernel"] = "rbf"
    sts.correct()
    assert sts["kernel"] == ["rbf"] * 2


def test_dim_getter(settings):
    assert settings.dim == 2


def test_dim_setter(settings):
    with pytest.raises(AttributeError):
        settings.dim = 3


def test_set_dependent_defaults(settings):
    sts = settings.copy()

    # Restore all dependent keywords to their defaults.
    # Use a non-periodic kernel and a gamma prior
    sts["kernel"] = ["rbf", "rbf"]
    sts.set_dependent_defaults(only_missing=False)
    assert sts["iterpts"] == 42
    # np.testing.assert_equal(sts["pp_iters"], np.arange(0, 43))
    assert sts['pp_iters'] is None
    assert sts["periods"] == approx(np.array([1.0, 1.0]))
    assert sts["min_dist_acqs"] == approx(0.01)
    np.testing.assert_array_equal(sts["pp_model_slice"], np.array([1, 2, 25]))

    # Only set defaults for None-valued, dependent keywords.
    # Use a periodic kernel
    sts = settings
    sts["kernel"] = ["stdp", "stdp"]
    sts["periods"] = np.array([0.5, 0.5])
    sts["iterpts"] = 100
    sts["pp_iters"] = np.arange(5)
    sts.set_dependent_defaults(only_missing=True)
    assert sts["periods"] == approx(np.array([0.5, 0.5]))
    assert sts["iterpts"] == 100
    np.testing.assert_array_equal(sts["pp_iters"], np.arange(5))
    assert sts["min_dist_acqs"] == approx(0.01 * 0.5)


def test_init():
    keywords = {
        "bounds": np.array([[0.0, 1.0], [0.0, 1.0]]),
        "kernel": "rbf",
    }
    sts = Settings(keywords)
    assert sts["bounds"] == approx(keywords["bounds"])
    assert sts["kernel"] == [keywords["kernel"]] * 2
    assert sts["iterpts"] == 42
    assert sts["thetaprior"] == "gamma"


def test_from_file(ipfile):
    sts = Settings.from_file(ipfile)
    assert sts.is_rst == False
    assert sts["bounds"] == approx(np.array([[0.0, 3.5], [1.0, 3.0]]))
    assert sts["kernel"] == ["stdp", "rbf"]
    assert sts["iterpts"] == 55
    assert sts["initpts"] == 10
    assert sts["yrange"] == approx(np.array([-5.0, 2.0]))
