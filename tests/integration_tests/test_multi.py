import numpy as np
import pytest

from boss.bo.bo_main import BOMain
from boss.utils.os import execute_in
from boss.testing.testfunc import get_test_func


@pytest.mark.integration
def test_multi_init(tmp_path):

    with execute_in(tmp_path):
        f = get_test_func("forrester")

        def f_low(x):
            return 0.5 * f(x) + 10 * (x - 5) - 5

        bo = BOMain(
            [f, f_low],
            bounds=f.bounds,
            yrange=f.f_range,
            task_initpts=[3, 20],
            iterpts=5,
            seed=22,
            optimtype="score",
        )
        res = bo.run()
        assert np.all(np.bincount(bo.model.inds) == [8, 20])

        mu_glmin = res.select("mu_glmin", -1)
        assert abs(mu_glmin - f.f_min) < 1e-2


@pytest.mark.integration
def test_multi_init_precomputed(tmp_path):

    with execute_in(tmp_path):
        f = get_test_func("forrester")

        def f_low(x):
            return 0.5 * f(x) + 10 * (x - 5) - 5

        # create initialisation data
        a = f.bounds[0][0]
        b = f.bounds[0][1]
        X = a + (b - a) * np.random.random(size=20)
        Y_lo = f_low(X).reshape(20, 1)
        X_lo = np.hstack((X.reshape(20, 1), np.full((20, 1), 1)))
        X = a + (b - a) * np.random.random(size=3)
        Y_hi = np.full((3, 1), np.nan)
        X_hi = np.hstack((X.reshape(3, 1), np.full((3, 1), 0)))
        Y_init = np.vstack((Y_hi, Y_lo))
        X_init = np.vstack((X_hi, X_lo))

        bo = BOMain(
            [f],
            bounds=f.bounds,
            yrange=f.f_range,
            num_tasks=2,
            iterpts=5,
            seed=22,
            optimtype="score",
        )
        res = bo.run(X_init=X_init, Y_init=Y_init)
        assert np.all(np.bincount(bo.model.inds) == [8, 20])

        mu_glmin = res.select("mu_glmin", -1)
        assert abs(mu_glmin - f.f_min) < 1e-2


@pytest.mark.integration
def test_multi_heuristic(tmp_path):

    with execute_in(tmp_path):
        f = get_test_func("forrester")

        def f_low(x):
            return 0.5 * f(x) + 10 * (x - 5) - 5

        bo = BOMain(
            [f, f_low],
            bounds=f.bounds,
            yrange=f.f_range,
            initpts=3,
            iterpts=100,
            acqfn_name="elcb_multi",
            task_cost=[1, 0.1],
            maxcost=10,
            seed=22,
            optimtype="score",
        )
        res = bo.run()
        assert res.num_iters < 25

        mu_glmin = res.select("mu_glmin", -1)
        assert abs(mu_glmin - f.f_min) < 1e-2
