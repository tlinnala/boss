import shutil
import subprocess
from pathlib import Path

import numpy as np
import pytest

import boss.io.parse as parse
from boss.bo.bo_main import BOMain
from boss.pp.pp_main import PPMain
from boss.settings import Settings
from boss.testing.testfunc import get_test_func
from boss.utils.os import execute_in


@pytest.mark.integration
def test_basic_1d(tmp_path):
    with execute_in(tmp_path):
        f = get_test_func("forrester")
        bo = BOMain(
            f,
            bounds=f.bounds,
            yrange=f.f_range,
            initpts=3,
            iterpts=7,
            seed=22,
            optimtype="score",
        )
        res = bo.run()

        mu_glmin = res.select("mu_glmin", -1)
        assert abs(mu_glmin - f.f_min) < 1e-2


@pytest.mark.integration
def test_pp_1d(tmp_path):
    with execute_in(tmp_path):
        f = get_test_func("styblinskitang", dim=1)
        bo = BOMain(
            f,
            bounds=f.bounds,
            yrange=f.f_range,
            initpts=3,
            iterpts=3,
            acqfn_name="lcb",
            optimtype="score",
        )
        res = bo.run()

        pp = PPMain(
            res,
            pp_acq_funcs=True,
            pp_models=True,
            pp_truef_at_glmins=True,
            pp_truef_npts=50,
        )
        pp.run()
        min_preds = np.loadtxt("postprocessing/minimum_predictions.dat")
        assert len(min_preds) == 4


@pytest.mark.integration
def test_cli_1d(tmp_path):
    with execute_in(tmp_path):
        initpts = 3
        iterpts = 4
        settings = Settings(
            keywords={
                "userfn": [f"{Path(__file__).parent / 'userfn_forrester.py'}"],
                "bounds": [0.0, 1.0],
                "yrange": [-10.0, 10.0],
                "initpts": initpts,
                "iterpts": iterpts,
                "acqfn_name": "elcb",
                "optimtype": "score",
                "pp_models": True,
                "pp_acq_funcs": True,
            }
        )
        # run from cli and do some sanity checks
        settings.dump("boss.in")
        compl_proc = subprocess.run(["boss", "-op", "boss.in"])
        compl_proc.check_returncode()
        min_preds = parse.parse_min_preds(settings, "boss.out")
        assert len(min_preds) == iterpts + 1

        # also rest restart
        shutil.move("boss.rst", "boss.in")
        compl_proc = subprocess.run(["boss", "-op", "boss.in"])
        compl_proc.check_returncode()
        min_preds = parse.parse_min_preds(settings, "boss.out")
        acqs, _ = parse.parse_acqs(settings, "boss.out")
        assert len(min_preds) == iterpts + 1
        assert len(acqs) == initpts + 2 * iterpts


@pytest.mark.integration
def test_resume(tmp_path):
    with execute_in(tmp_path):
        initpts = 3
        f = get_test_func("forrester")
        bo = BOMain(
            f,
            bounds=f.bounds,
            yrange=f.f_range,
            initpts=initpts,
            iterpts=3,
            seed=22,
            optimtype="score",
        )
        res = bo.run()

        # resume
        iterpts = 7
        res = bo.run(iterpts=iterpts)
        mu_glmin = res.select("mu_glmin", -1)
        assert abs(mu_glmin - f.f_min) < 1e-2
        assert len(res["X"]) == initpts + iterpts


@pytest.mark.integration
def test_restart(tmp_path):
    with execute_in(tmp_path):
        initpts = 3
        f = get_test_func("forrester")
        bo = BOMain(
            f,
            bounds=f.bounds,
            yrange=f.f_range,
            initpts=initpts,
            iterpts=3,
            seed=22,
            optimtype="score",
        )
        bo.run()

        # restart
        bo = BOMain.from_file(ipfile="boss.rst", outfile="boss.out", f=f)
        iterpts = 3
        res = bo.run(iterpts=iterpts)
        mu_glmin = res.select("mu_glmin", -1)
        assert abs(mu_glmin - f.f_min) < 1e-2
        assert len(res["mu_glmin"]) == iterpts + 1
        assert len(res["X"]) == initpts + 2 * iterpts


def test_conv_val(tmp_path):
    with execute_in(tmp_path):
        f = get_test_func("forrester")
        conv_iters = 5
        conv_reltol = 1e-3
        bo = BOMain(
            f,
            bounds=f.bounds,
            yrange=f.f_range,
            initpts=3,
            iterpts=100,
            seed=57,
            optimtype="score",
            convtype="glmin_val",
            conv_reltol=conv_reltol,
            conv_iters=conv_iters,
        )
        res = bo.run()

        mu_glmins = res["mu_glmin"].to_array()
        diffs = (
            np.abs(mu_glmins[1:] - mu_glmins[:-1])
            / np.maximum(np.abs(mu_glmins[1:]), np.abs(mu_glmins[:-1]))
        ) < conv_reltol
        assert np.all(diffs[-5:])
        assert abs(mu_glmins[-1] - f.f_min) < 1e-2
