from typing import *

import numpy as np
import pytest

from boss.bo.bo_main import BOMain
from boss.io.parse import parse_acqs
from boss.pp.pp_main import PPMain
from boss.testing.testfunc import TestFunc, get_test_func
from boss.utils.os import execute_in


class FakeSymmetryFunc:
    def __init__(self, test_func: TestFunc, batch_max: int = 3) -> None:
        self.parent = test_func
        self.batch_max = batch_max

    @property
    def bounds(self):
        return self.parent.bounds

    @property
    def frange(self):
        return self.parent.f_range

    def __call__(self, X: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
        f = self.parent
        n = np.random.randint(0, self.batch_max)
        X_new = (
            f.bounds[:, 0].T
            + np.random.random_sample((n, f.dim)) * (f.bounds[:, 1] - f.bounds[:, 0]).T
        )
        X_new = np.vstack((X, X_new))
        Y = f(X_new)
        return X_new, Y


@pytest.mark.integration
def test_userfn_symmetry(tmp_path):
    with execute_in(tmp_path):
        test_func = get_test_func("Himmelvalley")
        f = FakeSymmetryFunc(test_func, batch_max=3)

        initpts = 5
        iterpts = 5
        bo = BOMain(
            f,
            bounds=f.bounds,
            yrange=f.frange,
            initpts=initpts,
            iterpts=iterpts,
            optimtype="score",
            seed=2165,
        )
        res = bo.run()

        _, ind_lims = parse_acqs(bo.settings, "boss.out")
        bs0 = ind_lims[initpts]
        batch_sizes = np.insert(np.diff(ind_lims[initpts:]), 0, bs0)

        for i in range(iterpts):
            assert len(res.select("X", i)) == batch_sizes[i]

        # also test post-processing
        pp = PPMain(res, pp_models=True, pp_acq_funcs=True)
        pp.run()


@pytest.mark.integration
@pytest.mark.ygrad
def test_gradobs_1d(tmp_path):
    with execute_in(tmp_path):
        initpts = 3
        f = get_test_func("forrester")
        bo = BOMain(
            f.with_grad,
            ygrad=True,
            bounds=f.bounds,
            yrange=f.f_range,
            initpts=initpts,
            iterpts=3,
            seed=22,
            optimtype="score",
        )
        res = bo.run()

        mu_glmin = res.select("mu_glmin", -1)
        assert abs(mu_glmin - f.f_min) < 1e-2

        pp = PPMain(res, pp_acq_funcs=True, pp_models=True)
        pp.run()

        # restart
        bo = BOMain.from_file(ipfile="boss.rst", outfile="boss.out", f=f.with_grad)
        iterpts = 3
        res = bo.run(iterpts=iterpts)
        mu_glmin = res.select("mu_glmin", -1)
        assert abs(mu_glmin - f.f_min) < 1e-2
        assert len(res["mu_glmin"]) == iterpts + 1
        assert len(res["X"]) == initpts + 2 * iterpts


@pytest.mark.integration
@pytest.mark.ygrad
def test_resume_gradobs(tmp_path):
    with execute_in(tmp_path):
        initpts = 3
        f = get_test_func("forrester")
        bo = BOMain(
            f.with_grad,
            ygrad=True,
            bounds=f.bounds,
            yrange=f.f_range,
            initpts=initpts,
            iterpts=1,
            seed=22,
            optimtype="score",
        )
        res = bo.run()

        # resume
        iterpts = 2
        res = bo.run(iterpts=iterpts)
        mu_glmin = res.select("mu_glmin", -1)
        assert abs(mu_glmin - f.f_min) < 1e-2
        assert len(res["X"]) == initpts + iterpts


@pytest.mark.integration
def test_kriging_believer(tmp_path):
    with execute_in(tmp_path):
        initpts = 3
        f = get_test_func("forrester")
        batchpts = 2
        bo = BOMain(
            f,
            bounds=f.bounds,
            yrange=f.f_range,
            initpts=initpts,
            iterpts=0,
            seed=19872,
            optimtype="score",
            batchtype="kb",
            batchpts=batchpts,
            acqfn_name="lcb",
        )
        X_init, _ = bo.get_initpts()
        X_next = bo.init_run(X_init)
        acqfn = bo.acq_manager.acqfn
        model = bo.model

        assert X_next.shape == (batchpts, f.dim)

        # check that the 1st acq is the same as the acqfn would generate alone
        x_next0 = acqfn.minimize(f.bounds)
        assert np.allclose(x_next0, X_next[0])

        # check that the 2nd acq is the same as the intermediate acqfn would generate
        y_next0 = model.predict(x_next0)[0]
        model.add_data(x_next0, y_next0)
        model.optimize(2)
        
        x_next1 = acqfn.minimize(f.bounds)
        assert np.allclose(x_next1, X_next[1])

# Helper function for test_heteroscedastic_noise
def rastrigin_noisy(X):
    x = X[0, 0]
    noise = np.random.normal(scale = 0.1)
    return (10. + x**2 - 10*np.cos(2*np.pi*x)) * (1. + noise)

# Helper function for test_heteroscedastic_noise
def heteroscedastic_noise(hsc_args, **kwargs):
    mu_array, _ = kwargs["model"].predict(kwargs["X"])
    noise_array = hsc_args[0] * np.abs(mu_array)
    return noise_array

@pytest.mark.integration
def test_heteroscedastic_noise():
    bo = BOMain(
        rastrigin_noisy,
        model_name="gpy_hsc",
        bounds=np.array([[-5.12, 5.12]]),
        yrange=[0., 100.],
        kernel='rbf',
        initpts=6,
        iterpts=30,
        acqfn_name="elcb",
        hsc_noise=heteroscedastic_noise,
        hsc_args=[0.1],
        noise_init=[0.1]
        )
    res = bo.run()
    mu_glmin = res.select("mu_glmin", -1)
    assert abs(mu_glmin) < 0.1
