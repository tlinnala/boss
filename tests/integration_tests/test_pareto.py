import numpy as np
import pytest
from boss.utils.os import execute_in
from boss.bo.bo_main import BOMain
from boss.pp.pf_main import PFMain


@pytest.mark.integration
def test_pareto(tmp_path):
    with execute_in(tmp_path):
        bounds = np.array([[0.0, 3.0]])

        def f0(X):
            x = X[0, 0]
            return x**2

        def f1(X):
            x = X[0, 0]
            return -((x - 2) ** 2)

        # Run boss
        bo_MO = BOMain(
            [f0, f1],  # list all tasks
            bounds=bounds,
            num_tasks=2,
            kernel="rbf",
            iterpts=30,
            # task_initpts=[10, 10],
        )
        res = bo_MO.run()
        # Calculate the pareto front
        pf = PFMain(n_tasks=2, bounds=bounds, mesh_size=30, models=bo_MO)
        x_pf, y_pf, y_f = pf.get_pareto()
        assert abs(x_pf[0][0] - 2) < 1e-1
        assert abs(x_pf[-1][0] - 3) < 1e-1
        assert abs(y_pf[-1][0] - 9) < 1e-1
        assert abs(y_pf[-1][-1] - (-1)) < 1e-1


if __name__ == "__main__":
    pytest.main()
