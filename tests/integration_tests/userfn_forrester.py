from boss.testing.testfunc import get_test_func

def f(X):
    forest = get_test_func('forrester')
    return forest(X)
